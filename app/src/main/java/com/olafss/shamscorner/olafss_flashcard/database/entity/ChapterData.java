package com.olafss.shamscorner.olafss_flashcard.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "chapters")
public class ChapterData {

    /** chapter id */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    /** name of the chapter */
    @ColumnInfo(name = "name")
    private String mName;

    /** the subject part */
    @ColumnInfo(name = "part")
    private int mPart;

    /** the chapter number */
    @ColumnInfo(name = "number")
    private int mNumber;

    /**
     * construct chapter data from the chapter table in the database
     * @param mName chapter name
     * @param mPart subject part
     * @param mNumber chapter number
     */
    public ChapterData(String mName, int mPart, int mNumber) {
        this.mName = mName;
        this.mPart = mPart;
        this.mNumber = mNumber;
    }

    public int getId() {
        return this.mId;
    }

    public String getName() {
        return this.mName;
    }

    public int getPart() {
        return this.mPart;
    }

    public int getNumber() {
        return this.mNumber;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public void setPart(int mPart) {
        this.mPart = mPart;
    }

    public void setNumber(int mNumber) {
        this.mNumber = mNumber;
    }
}
