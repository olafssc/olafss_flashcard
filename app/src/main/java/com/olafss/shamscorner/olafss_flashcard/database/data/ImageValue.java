package com.olafss.shamscorner.olafss_flashcard.database.data;

import android.arch.persistence.room.ColumnInfo;

public class ImageValue {

    @ColumnInfo(name = "url")
    public String url;

    @ColumnInfo(name = "position")
    public int position;
}
