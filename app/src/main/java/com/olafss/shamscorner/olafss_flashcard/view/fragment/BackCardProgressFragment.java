package com.olafss.shamscorner.olafss_flashcard.view.fragment;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.data.ProgressValue;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.ProgressViewModel;
import com.olafss.shamscorner.olafss_flashcard.utilities.ConvertEngToBang;

/**
 * A simple {@link Fragment} subclass.
 */
public class BackCardProgressFragment extends Fragment {

    /**
     * The key ID of the chapter number
     */
    private static final String CHAPTER_NUMBER = "CHAPTER_NUMBER";

    /**
     * The key ID of the subject part
     */
    private static final String SUBJECT_PART = "CHAPTER_PART";

    /**
     * the chapter number and the subject part
     */
    private int chapterNumber, subjectPart;

    /**
     * the call back object of the interface to communicate
     */
    private OnTouchIKnowOrIDontKnowButton mCallback;

    /**
     * the touch interface for the i know and i don't know button
     */
    public interface OnTouchIKnowOrIDontKnowButton {
        void onTouchIKnow();

        void onTouchIDontKnow();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (OnTouchIKnowOrIDontKnowButton) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnTouchIKnowOrIDontKnowButton");
        }
    }

    public BackCardProgressFragment() {
        // Required empty public constructor
    }


    @SuppressLint("StaticFieldLeak")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            chapterNumber = savedInstanceState.getInt(CHAPTER_NUMBER);
            subjectPart = savedInstanceState.getInt(SUBJECT_PART);
        }

        // Inflate the back card fragment layout
        View rootView = inflater.inflate(R.layout.fragment_body_back, container, false);

        // initiate the view model for the progress value
        ProgressViewModel mProgressViewModel = ViewModelProviders.of(this).get(ProgressViewModel.class);

        // the chapter text view
        TextView tvChapter = rootView.findViewById(R.id.tv_chapter);

        // the subject part text view
        TextView tvPart = rootView.findViewById(R.id.tv_part);

        // set the chapter to the chapter text view
        String chapter = this.getResources().getString(R.string.chapter) +
                " : " + ConvertEngToBang.getNumberInBanglaFormat(chapterNumber);
        tvChapter.setText(chapter);

        // set the part to the part text view
        final String part = this.getResources().getString(R.string.part) +
                " : " + ConvertEngToBang.getNumberInBanglaFormat(subjectPart);
        tvPart.setText(part);

        // set the progress bars
        // find the new progressbar and set the value
        final ProgressBar progressBarNew = rootView.findViewById(R.id.new_state_bar);
        // find the corresponding percent label and set the text
        final TextView tvNewPercent = rootView.findViewById(R.id.new_state_percent);

        // find the learning progressbar and set the value
        final ProgressBar progressBarLearning = rootView.findViewById(R.id.learning_state_bar);
        // find the corresponding percent label and set the text
        final TextView tvLearningPercent = rootView.findViewById(R.id.learning_state_percent);

        // find the review progressbar and set the value
        final ProgressBar progressBarReview = rootView.findViewById(R.id.review_state_bar);
        // find the corresponding percent label and set the text
        final TextView tvReviewPercent = rootView.findViewById(R.id.review_state_percent);

        // find the master progressbar and set the value
        final ProgressBar progressBarMaster = rootView.findViewById(R.id.master_state_bar);
        // find the corresponding percent label and set the text
        final TextView tvMasterPercent = rootView.findViewById(R.id.master_state_percent);

        // now set the progress value
        new UpdateThingsAsyncTask(mProgressViewModel, chapterNumber, subjectPart) {
            @Override
            public void onResponseReceive(ProgressValue progressValue) {
                progressBarNew.setProgress(progressValue.newVal);
                String newPercent = progressValue.newVal + "%";
                tvNewPercent.setText(newPercent);

                progressBarLearning.setProgress(progressValue.learningVal);
                String learningPercent = progressValue.learningVal + "%";
                tvLearningPercent.setText(learningPercent);

                progressBarReview.setProgress(progressValue.reviewVal);
                String reviewPercent = progressValue.reviewVal + "%";
                tvReviewPercent.setText(reviewPercent);

                progressBarMaster.setProgress(progressValue.masterVal);
                String masterPercent = progressValue.masterVal + "%";
                tvMasterPercent.setText(masterPercent);
            }
        }.execute();

        // set the click event on the i know button
        Button btnIknow = rootView.findViewById(R.id.btn_i_know);
        btnIknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCallback.onTouchIKnow();
            }
        });

        // set the click event for the i don't know button
        Button btnIdontKnow = rootView.findViewById(R.id.btn_i_dont_know);
        btnIdontKnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCallback.onTouchIDontKnow();
            }
        });

        // return the root view
        return rootView;
    }

    private interface UpdateThings {
        void onResponseReceive(ProgressValue progressValue);
    }

    private static abstract class UpdateThingsAsyncTask extends AsyncTask<Void, Void, Void>
            implements UpdateThings {

        private ProgressViewModel progressViewModel;
        private ProgressValue progressValue;
        private int chapter;
        private int part;

        UpdateThingsAsyncTask(ProgressViewModel progressViewModel, int chapter, int part){
            this.progressViewModel = progressViewModel;
            this.chapter = chapter;
            this.part = part;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            progressValue = progressViewModel.getAProgressData(chapter, part);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            onResponseReceive(progressValue);
        }
    }

    /**
     * set the data for this fragment
     *
     * @param chapterNumber the chapter number
     * @param subjectPart   the subject part
     */
    public void setData(int chapterNumber, int subjectPart) {
        this.chapterNumber = chapterNumber;
        this.subjectPart = subjectPart;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle currentState) {
        currentState.putInt(CHAPTER_NUMBER, chapterNumber);
        currentState.putInt(SUBJECT_PART, subjectPart);
    }
}
