package com.olafss.shamscorner.olafss_flashcard.database.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.olafss.shamscorner.olafss_flashcard.database.FlashCardRoomDatabase;
import com.olafss.shamscorner.olafss_flashcard.database.dao.QuestionDataDao;
import com.olafss.shamscorner.olafss_flashcard.database.data.ChapterNameAndNumber;
import com.olafss.shamscorner.olafss_flashcard.database.data.ChapterNumberAndPart;
import com.olafss.shamscorner.olafss_flashcard.database.data.CountData;
import com.olafss.shamscorner.olafss_flashcard.database.data.ImageValue;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ChapterData;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ImageData;
import com.olafss.shamscorner.olafss_flashcard.database.entity.QuestionData;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class QuestionDataRepository {
    /**
     * add the dao object
     */
    private QuestionDataDao mQuestionDataDao;

    /**
     * construct the question data repository
     *
     * @param application the main application object
     */
    public QuestionDataRepository(Application application) {
        // get the database
        FlashCardRoomDatabase db = FlashCardRoomDatabase.getDatabase(application);

        // get and set the dao
        mQuestionDataDao = db.questionDataDao();
    }

    /**
     * get the chapter name and number as a list
     *
     * @param part the subject part
     * @return list of chapter name and number
     */
    public List<ChapterNameAndNumber> getChapterNameAndNumberList(int part) {
        return mQuestionDataDao.getChapterNameAndNumberList(part);
    }

    /**
     * get the number of questions with the specified chapter, part and state
     *
     * @param part    the subject part
     * @param chapter the chapter number
     * @param state   the current state (0-new, 1-learning, 2-review, 3-master)
     * @return the number of count of the questions
     */
    public int getProgressState(int part, int chapter, int state) {
        return mQuestionDataDao.getProgressState(part, chapter, state);
    }

    /**
     * get the total number of questions in the given chapter and part
     *
     * @param part    the subject part
     * @param chapter the chapter number
     * @return the total number of questions
     */
    public int getTotalQuestionCount(int part, int chapter) {
        return mQuestionDataDao.getTotalQuestionCount(part, chapter);
    }

    /**
     * update the questions state in the given question
     *
     * @param stateList the list of states
     */
    public void updateStateList(JSONArray stateList) {
        new updateStateListAsyncTask(mQuestionDataDao, stateList).execute();
    }

    /**
     * update the state list into the question table in the background
     */
    private static class updateStateListAsyncTask extends AsyncTask<Void, Void, Void> {

        /**
         * the question data dao
         */
        private QuestionDataDao mAsyncTaskDao;

        /**
         * json array for the states
         */
        private JSONArray stateList;

        updateStateListAsyncTask(QuestionDataDao mAsyncTaskDao, JSONArray stateList) {
            this.mAsyncTaskDao = mAsyncTaskDao;
            this.stateList = stateList;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // update the data list
            try {
                for (int i = 0; i < stateList.length(); i++) {
                    mAsyncTaskDao.updateStateList(
                            stateList.getJSONObject(i).getInt("question_id"),
                            stateList.getJSONObject(i).getInt("state"),
                            stateList.getJSONObject(i).getInt("i_know"),
                            stateList.getJSONObject(i).getInt("i_dont_know"),
                            stateList.getJSONObject(i).getInt("flipped"),
                            stateList.getJSONObject(i).getInt("skipped")
                    );
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * update the question paid column according to the question unique id
     *
     * @param paidList the list of question id and the paid column
     * @param isPaid the paid or not - boolean
     */
    public void updateIdAndPaidQuestions(JSONArray paidList, boolean isPaid) {
        new updateQuestionPaidAsyncTask(mQuestionDataDao,paidList, isPaid).execute();
    }

    private static class updateQuestionPaidAsyncTask extends AsyncTask<Void, Void, Void> {

        /**
         * the question data dao
         */
        private QuestionDataDao mAsyncTaskDao;

        /**
         * json array for the states
         */
        private JSONArray paidList;

        /**
         * paid or not
         */
        private boolean isPaid;

        updateQuestionPaidAsyncTask(QuestionDataDao mAsyncTaskDao, JSONArray paidList, boolean isPaid) {
            this.mAsyncTaskDao = mAsyncTaskDao;
            this.paidList = paidList;
            this.isPaid = isPaid;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // update the data list

            try {
                if (isPaid) {
                    for (int i = 0; i < paidList.length(); i++) {
                        mAsyncTaskDao.updateIdAndPaidQuestions(
                                paidList.getJSONObject(i).getInt("id"),
                                1
                        );
                    }

                } else {
                    for (int i = 0; i < paidList.length(); i++) {
                        mAsyncTaskDao.updateIdAndPaidQuestions(
                                paidList.getJSONObject(i).getInt("id"),
                                paidList.getJSONObject(i).getInt("paid")
                        );
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * delete the current chapter data
     */
    public void deleteChapterData() {
        new DeleteChapterDataAsyncTask(mQuestionDataDao).execute();
    }

    /**
     * delete the current question data
     */
    public void deleteQuestionData() {
        new DeleteQuestionDataAsyncTask(mQuestionDataDao).execute();
    }

    /**
     * delete the current image data
     */
    public void deleteImageData() {
        new DeleteImageDataAsyncTask(mQuestionDataDao).execute();
    }

    private static class DeleteQuestionDataAsyncTask extends AsyncTask<Void, Void, Void> {

        private QuestionDataDao dataDao;

        DeleteQuestionDataAsyncTask(QuestionDataDao dataDao) {
            this.dataDao = dataDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            dataDao.deleteQuestionData();

            return null;
        }
    }

    private static class DeleteChapterDataAsyncTask extends AsyncTask<Void, Void, Void> {

        private QuestionDataDao dataDao;

        DeleteChapterDataAsyncTask(QuestionDataDao dataDao) {
            this.dataDao = dataDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            dataDao.deleteChapterData();

            return null;
        }
    }

    private static class DeleteImageDataAsyncTask extends AsyncTask<Void, Void, Void> {

        private QuestionDataDao dataDao;

        DeleteImageDataAsyncTask(QuestionDataDao dataDao) {
            this.dataDao = dataDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            dataDao.deleteImageData();

            return null;
        }
    }

    /**
     * insert the chapter data into the chapter table
     */
    public void insertChapterList(JSONArray chapterDataList) {
        new insertAsyncTaskForChapterData(mQuestionDataDao, chapterDataList).execute();
    }

    /**
     * insert the question data into the question table
     */
    public void insertQuestionList(JSONArray questionDataList, boolean isPaid) {
        new insertAsyncTaskForQuestionData(mQuestionDataDao, questionDataList, isPaid).execute();
    }

    /**
     * insert the image data into the image table
     */
    public void insertImageList(JSONArray imageDataList) {
        new insertAsyncTaskForImageData(mQuestionDataDao, imageDataList).execute();
    }

    /**
     * insert the question data in the question table in background
     */
    private static class insertAsyncTaskForQuestionData extends AsyncTask<Void, Void, Void> {

        /**
         * the progress data dao
         */
        private QuestionDataDao mAsyncTaskDao;

        /**
         * list of progress data
         */
        private JSONArray dataList;

        /**
         * paid or not
         */
        private boolean isPaid;

        insertAsyncTaskForQuestionData(QuestionDataDao mAsyncTaskDao, JSONArray dataList, boolean isPaid) {
            this.mAsyncTaskDao = mAsyncTaskDao;
            this.dataList = dataList;
            this.isPaid = isPaid;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // insert the data list
            try {
                // list of question data
                List<QuestionData> questionDataList = new ArrayList<>();

                if (isPaid) {

                    for (int i = 0; i < dataList.length(); i++) {

                        // add the question into the question data list
                        questionDataList.add(new QuestionData(
                                dataList.getJSONObject(i).getInt("id"),
                                dataList.getJSONObject(i).getString("name"),
                                dataList.getJSONObject(i).getString("answer"),
                                0,
                                0,
                                0,
                                0,
                                0,
                                dataList.getJSONObject(i).getInt("part"),
                                dataList.getJSONObject(i).getInt("chapter_number"),
                                1));
                    }

                } else {
                    for (int i = 0; i < dataList.length(); i++) {

                        // add the question into the question data list
                        questionDataList.add(new QuestionData(
                                dataList.getJSONObject(i).getInt("id"),
                                dataList.getJSONObject(i).getString("name"),
                                dataList.getJSONObject(i).getString("answer"),
                                0,
                                0,
                                0,
                                0,
                                0,
                                dataList.getJSONObject(i).getInt("part"),
                                dataList.getJSONObject(i).getInt("chapter_number"),
                                dataList.getJSONObject(i).getInt("paid")));
                    }
                }

                // insert the data
                mAsyncTaskDao.insertQuestionList(questionDataList);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * insert the chapter data in the chapter table in background
     */
    private static class insertAsyncTaskForChapterData extends AsyncTask<Void, Void, Void> {

        /**
         * the progress data dao
         */
        private QuestionDataDao mAsyncTaskDao;

        /**
         * list of progress data
         */
        private JSONArray dataList;

        insertAsyncTaskForChapterData(QuestionDataDao mAsyncTaskDao, JSONArray dataList) {
            this.mAsyncTaskDao = mAsyncTaskDao;
            this.dataList = dataList;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // insert the data list
            try {
                // list of chapter data
                List<ChapterData> chapterDataList = new ArrayList<>();

                for (int i = 0; i < dataList.length(); i++) {
                    // add the question into the question data list
                    chapterDataList.add(new ChapterData(
                            dataList.getJSONObject(i).getString("name"),
                            dataList.getJSONObject(i).getInt("part"),
                            dataList.getJSONObject(i).getInt("number")));
                }

                // insert the data
                mAsyncTaskDao.insertChapterList(chapterDataList);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * insert the image data in the image table in background
     */
    private static class insertAsyncTaskForImageData extends AsyncTask<Void, Void, Void> {

        /**
         * the progress data dao
         */
        private QuestionDataDao mAsyncTaskDao;

        /**
         * list of progress data
         */
        private JSONArray dataList;

        insertAsyncTaskForImageData(QuestionDataDao mAsyncTaskDao, JSONArray dataList) {
            this.mAsyncTaskDao = mAsyncTaskDao;
            this.dataList = dataList;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // insert the data list
            try {
                // list of chapter data
                List<ImageData> imageDataList = new ArrayList<>();

                for (int i = 0; i < dataList.length(); i++) {
                    // add the question into the question data list
                    imageDataList.add(new ImageData(
                            dataList.getJSONObject(i).getInt("id"),
                            dataList.getJSONObject(i).getString("url"),
                            dataList.getJSONObject(i).getInt("type"),
                            dataList.getJSONObject(i).getInt("position")));
                }

                // insert the data
                mAsyncTaskDao.insertImageList(imageDataList);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * reset the current question states for the chapter and part
     *
     * @param chapter the subject chapter
     * @param part    the subject part
     */
    public void resetQuestionState(int chapter, int part) {
        new ResetQuestionStateAsyncTask(mQuestionDataDao, chapter, part).execute();
    }

    private static class ResetQuestionStateAsyncTask extends AsyncTask<Void, Void, Void> {

        private QuestionDataDao mAsyncTask;
        private int chapter;
        private int part;

        ResetQuestionStateAsyncTask(QuestionDataDao mAsyncTask, int chapter, int part) {
            this.mAsyncTask = mAsyncTask;
            this.chapter = chapter;
            this.part = part;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // reset the data
            mAsyncTask.resetQuestionState(chapter, part);

            return null;
        }
    }

    /**
     * get the question data according to the given chapter and part
     *
     * @param chapter the chapter number
     * @param part    the subject part
     * @return the question data object
     */
    public QuestionData getQuestion(int chapter, int part) {
        return mQuestionDataDao.getQuestion(chapter, part);
    }

    /**
     * update the state of the given question id
     *
     * @param id  the question id
     * @param val the value
     */
    public void updateState(int id, int val) {
        new UpdateStateAsyncTask(mQuestionDataDao, id, val).execute();
    }

    private static class UpdateStateAsyncTask extends AsyncTask<Void, Void, Void> {

        private QuestionDataDao mAsyncTask;
        private int id;
        private int val;

        UpdateStateAsyncTask(QuestionDataDao mAsyncTask, int id, int val) {
            this.mAsyncTask = mAsyncTask;
            this.id = id;
            this.val = val;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // update value
            mAsyncTask.updateState(id, val);

            return null;
        }
    }

    /**
     * update the flipped counter of the given question id
     *
     * @param id  the question id
     * @param val the value
     */
    public void updateFlipped(int id, int val) {
        new UpdateFlippedAsyncTask(mQuestionDataDao, id, val).execute();
    }

    private static class UpdateFlippedAsyncTask extends AsyncTask<Void, Void, Void> {

        private QuestionDataDao mAsyncTask;
        private int id;
        private int val;

        UpdateFlippedAsyncTask(QuestionDataDao mAsyncTask, int id, int val) {
            this.mAsyncTask = mAsyncTask;
            this.id = id;
            this.val = val;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // update value
            mAsyncTask.updateFlipped(id, val);

            return null;
        }
    }

    /**
     * update the skipped counter of the given question id
     *
     * @param id  the question id
     * @param val the value
     */
    public void updateSkipped(int id, int val) {
        new UpdateSkippedAsyncTask(mQuestionDataDao, id, val).execute();
    }

    private static class UpdateSkippedAsyncTask extends AsyncTask<Void, Void, Void> {

        private QuestionDataDao mAsyncTask;
        private int id;
        private int val;

        UpdateSkippedAsyncTask(QuestionDataDao mAsyncTask, int id, int val) {
            this.mAsyncTask = mAsyncTask;
            this.id = id;
            this.val = val;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // update value
            mAsyncTask.updateSkipped(id, val);

            return null;
        }
    }

    /**
     * update the i know counter of the given question id
     *
     * @param id  the question id
     * @param val the value
     */
    public void updateiKnow(int id, int val) {
        new UpdateIKnowAsyncTask(mQuestionDataDao, id, val).execute();
    }

    private static class UpdateIKnowAsyncTask extends AsyncTask<Void, Void, Void> {

        private QuestionDataDao mAsyncTask;
        private int id;
        private int val;

        UpdateIKnowAsyncTask(QuestionDataDao mAsyncTask, int id, int val) {
            this.mAsyncTask = mAsyncTask;
            this.id = id;
            this.val = val;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // update value
            mAsyncTask.updateIKnow(id, val);

            return null;
        }
    }

    /**
     * update the i don't know counter of the given question id
     *
     * @param id  the question id
     * @param val the value
     */
    public void updateIDontKnow(int id, int val) {
        new UpdateIDontKnowAsyncTask(mQuestionDataDao, id, val).execute();
    }

    private static class UpdateIDontKnowAsyncTask extends AsyncTask<Void, Void, Void> {

        private QuestionDataDao mAsyncTask;
        private int id;
        private int val;

        UpdateIDontKnowAsyncTask(QuestionDataDao mAsyncTask, int id, int val) {
            this.mAsyncTask = mAsyncTask;
            this.id = id;
            this.val = val;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // update value
            mAsyncTask.updateIDontKnow(id, val);

            return null;
        }
    }

    /**
     * get the image data from the given id and type, front or back
     *
     * @param id      the question id
     * @param isFront front or back
     * @return image value (url, position)
     */
    public ImageValue getImageData(int id, int isFront) {
        return mQuestionDataDao.getImageData(id, isFront);
    }

    /**
     * get the total number of count of count data object (i know, i don't know, skipped, flipped)
     *
     * @return the counter of each items in the count data object
     */
    public LiveData<CountData> getCountData() {
        return mQuestionDataDao.getCountData();
    }

    /**
     * get the recommendation for the recommend panel
     *
     * @return list of chapters
     */
    public LiveData<List<ChapterNumberAndPart>> getRecommendations() {
        return mQuestionDataDao.getRecommendations();
    }
}
