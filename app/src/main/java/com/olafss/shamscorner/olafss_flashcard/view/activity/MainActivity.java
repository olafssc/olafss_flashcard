package com.olafss.shamscorner.olafss_flashcard.view.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.data.NameAndLevel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.UserViewModel;
import com.olafss.shamscorner.olafss_flashcard.utilities.BottomNavigationViewBehavior;
import com.olafss.shamscorner.olafss_flashcard.utilities.SessionManager;
import com.olafss.shamscorner.olafss_flashcard.view.fragment.BuyNowDialog;
import com.olafss.shamscorner.olafss_flashcard.view.fragment.GroupFragment;
import com.olafss.shamscorner.olafss_flashcard.view.fragment.HomeFragment;
import com.olafss.shamscorner.olafss_flashcard.view.fragment.OthersFragment;
import com.olafss.shamscorner.olafss_flashcard.view.fragment.ProfileEditDialogFragment;
import com.olafss.shamscorner.olafss_flashcard.view.fragment.ProfileFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //private static final String TAG = MainActivity.class.getSimpleName();

    private ActionBar mainToolBar;

    /**
     * view model for the user table
     */
    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get the view model
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        /*if (!session.isLoggedIn()) {
            logoutUser();
        }*/

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // get and set the action on navigation view
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // set the name and the level in the nav header main
        setNameAndLevel(navigationView);

        // bottom navigation view
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationViewBehavior());

        // load the home fragment by default
        loadFragment(new HomeFragment());

        // set the preload toolbar label as home
        mainToolBar = getSupportActionBar();
        if (mainToolBar != null) {
            mainToolBar.setTitle("Home");
        }
    }

    /**
     * set the name and level in the nav header main
     *
     * @param navigationView the parent navigation view holder
     */
    private void setNameAndLevel(NavigationView navigationView) {
        // get the nav header main layout first
        View navHeaderView = navigationView.getHeaderView(0);
        // now get the textview inside the nav header view
        final TextView tvName = navHeaderView.findViewById(R.id.tv_name);
        final TextView tvLevel = navHeaderView.findViewById(R.id.tv_level);

        // update these views as view model
        userViewModel.getNameAndLevel().observe(this, new Observer<NameAndLevel>() {
            @Override
            public void onChanged(@Nullable NameAndLevel nameAndLevel) {

                if (nameAndLevel != null) {
                    tvName.setText(nameAndLevel.name);
                    switch (nameAndLevel.level) {
                        case 0:
                            tvLevel.setText(R.string.level_beginner);
                            break;
                        case 1:
                            tvLevel.setText(R.string.level_advanced);
                            break;
                        case 2:
                            tvLevel.setText(R.string.level_professional);
                            break;
                    }
                }
            }
        });
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     */
    private void logoutUser() {
        // session manager
        SessionManager session = new SessionManager(this);

        // set the session state false
        session.setLogin(false);

        // clear the user table
        userViewModel.deleteUserData();

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, Login.class);
        startActivity(intent);
        finish();
    }

    /**
     * this method get the specified fragment and load into the activity container
     *
     * @param fragment the current fragment
     */
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mainToolBar.setTitle("Home");
                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_profile:
                    mainToolBar.setTitle("Profile");
                    fragment = new ProfileFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_others:
                    mainToolBar.setTitle("Others");
                    fragment = new OthersFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_group:
                    mainToolBar.setTitle("Group");
                    fragment = new GroupFragment();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // exit the application
            finish();
            System.exit(0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_buy_now:
                // open the buy now dialog
                new BuyNowDialog().show(getSupportFragmentManager(), "dialog_buy");
                return true;
            case R.id.action_logout:
                // perform the logout action
                logoutUser();
                return true;

            case R.id.action_edit_profile:
                // perform the edit profile action
                editProfile();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * edit the profile
     */
    private void editProfile() {
        // open a full screen dialog
        ProfileEditDialogFragment dialog = new ProfileEditDialogFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        dialog.show(transaction, ProfileEditDialogFragment.TAG);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
