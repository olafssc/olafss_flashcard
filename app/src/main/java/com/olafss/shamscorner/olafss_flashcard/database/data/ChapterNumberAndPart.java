package com.olafss.shamscorner.olafss_flashcard.database.data;

import android.arch.persistence.room.ColumnInfo;

public class ChapterNumberAndPart {
    /** the chapter number */
    @ColumnInfo(name = "chapter")
    public int chapter;

    /** the subject part */
    @ColumnInfo(name = "part")
    public int part;
}
