package com.olafss.shamscorner.olafss_flashcard.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "progress")
public class ProgressData {

    /** progress id */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    /** the subject part */
    @ColumnInfo(name = "part")
    private int mPart;

    /** the chapter number */
    @ColumnInfo(name = "chapter_number")
    private int mChapterNumber;

    /** the chapter name */
    @ColumnInfo(name = "chapter_name")
    private String mChapterName;

    /** the progress new value */
    @ColumnInfo(name = "new")
    private int mProgressNew;

    /** the progress learning value */
    @ColumnInfo(name = "learning")
    private int mProgressLearning;

    /** the progress review value */
    @ColumnInfo(name = "review")
    private int mProgressReview;

    /** the progress master value */
    @ColumnInfo(name = "master")
    private int mProgressMaster;

    /**
     * construct the progress data from the progress table in the database
     * @param mPart the subject part
     * @param mChapterNumber the chapter number
     * @param mChapterName the chapter name
     * @param mProgressNew the progress new value
     * @param mProgressLearning the progress learning value
     * @param mProgressReview the progress review value
     * @param mProgressMaster the progress master value
     */
    public ProgressData(int mPart, int mChapterNumber, String mChapterName, int mProgressNew,
                        int mProgressLearning, int mProgressReview, int mProgressMaster) {

        this.mPart = mPart;
        this.mChapterNumber = mChapterNumber;
        this.mChapterName = mChapterName;
        this.mProgressNew = mProgressNew;
        this.mProgressLearning = mProgressLearning;
        this.mProgressReview = mProgressReview;
        this.mProgressMaster = mProgressMaster;
    }

    public int getId() {
        return this.mId;
    }

    public int getPart() {
        return this.mPart;
    }

    public int getChapterNumber() {
        return this.mChapterNumber;
    }

    public String getChapterName() {
        return this.mChapterName;
    }

    public int getProgressNew() {
        return this.mProgressNew;
    }

    public int getProgressLearning() {
        return this.mProgressLearning;
    }

    public int getProgressReview() {
        return this.mProgressReview;
    }

    public int getProgressMaster() {
        return this.mProgressMaster;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public void setPart(int mPart) {
        this.mPart = mPart;
    }

    public void setChapterNumber(int mChapterNumber) {
        this.mChapterNumber = mChapterNumber;
    }

    public void setChapterName(String mChapterName) {
        this.mChapterName = mChapterName;
    }

    public void setProgressNew(int mProgressNew) {
        this.mProgressNew = mProgressNew;
    }

    public void setProgressLearning(int mProgressLearning) {
        this.mProgressLearning = mProgressLearning;
    }

    public void setProgressReview(int mProgressReview) {
        this.mProgressReview = mProgressReview;
    }

    public void setProgressMaster(int mProgressMaster) {
        this.mProgressMaster = mProgressMaster;
    }
}
