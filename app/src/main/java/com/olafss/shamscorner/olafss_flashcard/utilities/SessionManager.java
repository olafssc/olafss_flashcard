package com.olafss.shamscorner.olafss_flashcard.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    private SharedPreferences pref;

    // Shared pref mode
    private static final int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "olafss_flashcard_login";

    // the key for the logged in or not
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    // the key for the first time launch or not
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public SessionManager(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    /**
     * set the login value as logged in or not
     *
     * @param isLoggedIn checks is logged in or not
     */
    public void setLogin(boolean isLoggedIn) {

        // get the editor
        SharedPreferences.Editor editor = pref.edit();

        // put the value to the editor
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // apply changes
        editor.apply();

        Log.d(TAG, "User login session modified!");
    }

    /**
     * checks whether logged in ot not
     *
     * @return whether logged in ot not - boolean value
     */
    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    /**
     * set the first time launch
     *
     * @param isFirstTime whether it first time or not
     */
    public void setFirstTimeLaunch(boolean isFirstTime) {
        // get the editor
        SharedPreferences.Editor editor = pref.edit();

        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);

        editor.apply();
    }

    /**
     * checks is it first time launch or not
     *
     * @return the first time or not - boolean
     */
    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
}
