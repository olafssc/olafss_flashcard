package com.olafss.shamscorner.olafss_flashcard.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.olafss.shamscorner.olafss_flashcard.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FrontCardButtonFragment extends Fragment {

    /** interface to communicate */
    private OnTouchToSeeClickListener mCallback;

    /** the touch to see click listener */
    public interface OnTouchToSeeClickListener{
        void onTouchToSeeClicked();
        void onSkipClicked();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mCallback = (OnTouchToSeeClickListener) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString()
                    + " must implement OnTouchToSeeClickListener");
        }
    }

    public FrontCardButtonFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the front card fragment layout
        View rootView = inflater.inflate(R.layout.fragment_body_front, container, false);

        // the click event on the touch to see the answer button
        Button btnTouchToSee = rootView.findViewById(R.id.btn_touch_to_see);
        btnTouchToSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCallback.onTouchToSeeClicked();
            }
        });

        // the click event on the skip button
        Button btnSkipped = rootView.findViewById(R.id.btn_skip);
        btnSkipped.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCallback.onSkipClicked();
            }
        });

        // return the root view
        return rootView;
    }
}
