package com.olafss.shamscorner.olafss_flashcard.view.activity;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.ProgressViewModel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.QuestionViewModel;

public class FinishedActivity extends AppCompatActivity {

    /** The key ID of the chapter number */
    private static final String CHAPTER_NUMBER =  "CHAPTER_NUMBER";

    /** The key ID of the subject part */
    private static final String SUBJECT_PART =  "CHAPTER_PART";

    /** the subject chapter */
    private int subjectChapter;

    /** the subject part */
    private int subjectPart;

    /** the question data view model */
    private QuestionViewModel mQuestionViewModel;

    /** the user data view model */
    private ProgressViewModel mProgressViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finished);

        // get the chapter number given from the previous activity
        // get the bundle extras first from the current activity
        Bundle extras = getIntent().getExtras();
        // check whether the extras are empty or not, if not then get the number
        if(extras != null){
            subjectChapter = extras.getInt(CHAPTER_NUMBER);
            subjectPart = extras.getInt(SUBJECT_PART);
        }

        // get the view models
        mQuestionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);
        mProgressViewModel = ViewModelProviders.of(this).get(ProgressViewModel.class);

        // set the action event for the back to home button
        findViewById(R.id.btn_back_to_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToHome();
            }
        });

        // set the action event for the play again button
        findViewById(R.id.btn_play_again).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UpdateThingsInBackgroundForPlayAgain().execute();
            }
        });
    }

    /**
     * the background process for the play again button action
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateThingsInBackgroundForPlayAgain extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            // reset the state of the specified chapter and part of a question in the questions table
            mQuestionViewModel.resetQuestionState(subjectChapter, subjectPart);

            // reset the progress table of the specified chapter and part
            mProgressViewModel.resetProgressData(subjectChapter, subjectPart);

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            // pass the chapter number to the next activity using extras
            Intent frontCardIntent = new Intent(getApplicationContext(), CardActivity.class);
            frontCardIntent.putExtra(CHAPTER_NUMBER, subjectChapter);
            frontCardIntent.putExtra(SUBJECT_PART, subjectPart);
            startActivity(frontCardIntent);
            // make transition push left
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        backToHome();
    }

    /**
     * get back to the home activity
     */
    private void backToHome(){
        Intent setIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(setIntent);
        finish();
    }
}
