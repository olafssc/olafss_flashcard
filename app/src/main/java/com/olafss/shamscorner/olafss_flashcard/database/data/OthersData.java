package com.olafss.shamscorner.olafss_flashcard.database.data;

public class OthersData {
    /**
     * the title text of an item
     */
    public String text;

    /**
     * the corresponding image for the item
     */
    public int imageId;

    public OthersData(String text, int imageId) {
        this.text = text;
        this.imageId = imageId;
    }
}
