package com.olafss.shamscorner.olafss_flashcard.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.olafss.shamscorner.olafss_flashcard.R;

public class SampleDemo extends AppCompatActivity {

    private static final String TAG = SampleDemo.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_demo);
    }
}
