package com.olafss.shamscorner.olafss_flashcard.view.activity.payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.baoyachi.stepview.HorizontalStepView;
import com.baoyachi.stepview.VerticalStepView;
import com.baoyachi.stepview.bean.StepBean;
import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.utilities.GoHome;

import java.util.ArrayList;
import java.util.List;

public class PaymentActivity2 extends AppCompatActivity {

    /**
     * the payment option
     */
    private String option;

    /**
     * the key id of the selected option
     */
    private static final String SELECTED_OPTION = "SELECTED_OPTION";

    /**
     * the next button
     */
    private Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_step_2);

        // set the steps view
        HorizontalStepView setpview5 = findViewById(R.id.step_view);
        List<StepBean> stepsBeanList = new ArrayList<>();
        StepBean stepBean0 = new StepBean("Choose",1);
        StepBean stepBean1 = new StepBean("Pay",-1);
        StepBean stepBean2 = new StepBean("Enter",-1);
        StepBean stepBean3 = new StepBean("Go",-1);
        stepsBeanList.add(stepBean0);
        stepsBeanList.add(stepBean1);
        stepsBeanList.add(stepBean2);
        stepsBeanList.add(stepBean3);
        setpview5
                .setStepViewTexts(stepsBeanList)
                .setTextSize(12)//set textSize
                .setStepsViewIndicatorCompletedLineColor(getResources().getColor(R.color.colorAccent))
                .setStepsViewIndicatorUnCompletedLineColor(getResources().getColor(R.color.colorAccent))
                .setStepViewComplectedTextColor(getResources().getColor(R.color.colorAccent))
                .setStepViewUnComplectedTextColor(getResources().getColor(R.color.colorAccent))
                .setStepsViewIndicatorCompleteIcon(getResources().getDrawable(R.drawable.icon_check_round))
                .setStepsViewIndicatorDefaultIcon(getResources().getDrawable(R.drawable.icon_cancel_round))
                .setStepsViewIndicatorAttentionIcon(getResources().getDrawable(R.drawable.icon_replay));

        // get the bundle extras first from the current activity
        Bundle extras = getIntent().getExtras();
        // check whether the extras are empty or not, if not then get the number
        if (extras != null) {
            option = extras.getString(SELECTED_OPTION);
        }

        // get the image view of the qr code
        ImageView imgQrCode = findViewById(R.id.img_qr_code);

        // get the instruction showing step views
        VerticalStepView verticalStepViewDial = findViewById(R.id.step_view_for_instruction_dial);
        VerticalStepView verticalStepViewApp = findViewById(R.id.step_view_for_instruction_app);

        // decide which list to show
        if (option.equals("bkash")) {
            // set the header and logo
            TextView tvHeader = findViewById(R.id.text_header);
            tvHeader.setText(getResources().getString(R.string.instructions_for_bkash_payment));

            ImageView imgLogo = findViewById(R.id.logo);
            imgLogo.setBackgroundResource(R.drawable.logo_bkash);

            TextView tvHeaderDial = findViewById(R.id.dial_header);
            tvHeaderDial.setText(getResources().getString(R.string.using_bkash_dial));

            TextView tvHeaderApp = findViewById(R.id.app_header);
            tvHeaderApp.setText(getResources().getString(R.string.using_bkash_app));

            // now prepare for the list items
            List<String> instructionListDial = new ArrayList<>();
            instructionListDial.add(getResources().getString(R.string.use_keypad_to_enter_247_and_call));
            instructionListDial.add(getResources().getString(R.string.choose_option_3_payment));
            instructionListDial.add(getResources().getString(R.string.enter_merchant_bkash_number_01733412606));
            instructionListDial.add(getResources().getString(R.string.enter_the_amount_100));
            instructionListDial.add(getResources().getString(R.string.enter_the_reference_number_1));
            instructionListDial.add(getResources().getString(R.string.enter_the_counter_number_1));
            instructionListDial.add(getResources().getString(R.string.enter_your_pin_number));
            instructionListDial.add(getResources().getString(R.string.almost_done_you_will_now_receive_a_trxid_write_it_down));

            // set the list in the step view
            setListItemForInstruction(verticalStepViewDial, instructionListDial);


            // clear and the new list items
            List<String> instructionListApp = new ArrayList<>();
            instructionListApp.add(getResources().getString(R.string.login_to_bkash));
            instructionListApp.add(getResources().getString(R.string.select_payment_icon));
            instructionListApp.add(getResources().getString(R.string.enter_merchant_bkash_number_01733412606_app));
            instructionListApp.add(getResources().getString(R.string.enter_the_amount_100));
            instructionListApp.add(getResources().getString(R.string.enter_your_pin_number));
            instructionListApp.add(getResources().getString(R.string.tap_and_hold_to_pay));
            instructionListApp.add(getResources().getString(R.string.almost_done_you_will_now_receive_a_trxid_write_it_down));

            // set the list in the step view
            setListItemForInstruction(verticalStepViewApp, instructionListApp);

            // show the qr code
            imgQrCode.setVisibility(View.VISIBLE);

        } else if (option.equals("rocket")) {
            // set the header and logo
            TextView tvHeader = findViewById(R.id.text_header);
            tvHeader.setText(getResources().getString(R.string.instructions_for_rocket_payment));

            ImageView imgLogo = findViewById(R.id.logo);
            imgLogo.setBackgroundResource(R.drawable.logo_rocket);

            TextView tvHeaderDial = findViewById(R.id.dial_header);
            tvHeaderDial.setText(getResources().getString(R.string.using_rocket_dial));

            TextView tvHeaderApp = findViewById(R.id.app_header);
            tvHeaderApp.setText(getResources().getString(R.string.using_rocket_app));

            // now prepare for the list items
            List<String> instructionListDial = new ArrayList<>();
            instructionListDial.add(getResources().getString(R.string.use_keypad_to_enter_322_and_call));
            instructionListDial.add(getResources().getString(R.string.choose_option_1_payment));
            instructionListDial.add(getResources().getString(R.string.choose_option_2_merchant));
            instructionListDial.add(getResources().getString(R.string.enter_merchant_rocket_number_017334126067));
            instructionListDial.add(getResources().getString(R.string.enter_the_reference_number_1));
            instructionListDial.add(getResources().getString(R.string.enter_the_amount_100));
            instructionListDial.add(getResources().getString(R.string.enter_your_pin_number));
            instructionListDial.add(getResources().getString(R.string.almost_done_you_will_now_receive_a_trxid_write_it_down));

            // set the list in the step view
            setListItemForInstruction(verticalStepViewDial, instructionListDial);

            // clear and add the new list items
            List<String> instructionListApp = new ArrayList<>();
            instructionListApp.clear();
            instructionListApp.add(getResources().getString(R.string.login_to_rocket));
            instructionListApp.add(getResources().getString(R.string.select_merchant_pay_icon));
            instructionListApp.add(getResources().getString(R.string.enter_merchant_rocket_number_017334126067));
            instructionListApp.add(getResources().getString(R.string.enter_the_reference_number_1));
            instructionListApp.add(getResources().getString(R.string.enter_the_amount_100));
            instructionListApp.add(getResources().getString(R.string.enter_your_pin_number));
            instructionListApp.add(getResources().getString(R.string.click_on_the_pay_button));
            instructionListApp.add(getResources().getString(R.string.almost_done_you_will_now_receive_a_trxid_write_it_down));

            // set the list in the step view
            setListItemForInstruction(verticalStepViewApp, instructionListApp);

            // show the qr code
            imgQrCode.setVisibility(View.GONE);
        }

        // get the next button
        btnNext = findViewById(R.id.btn_next);
        // make the next button disabled
        btnNext.setEnabled(false);
        btnNext.setTextColor(getResources().getColor(android.R.color.darker_gray));
    }

    @Override
    public void onBackPressed() {
        new GoHome(this).showDialog();
    }

    /**
     * the yes radio button handler
     *
     * @param view the yes radio button
     */
    public void radioYesNo(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_yes:
                if (checked){
                    // make the next button enabled
                    btnNext.setEnabled(true);
                    btnNext.setTextColor(getResources().getColor(R.color.colorAccent));
                }
                break;
            case R.id.radio_no:
                if (checked){
                    // make the next button disabled
                    btnNext.setEnabled(false);
                    btnNext.setTextColor(getResources().getColor(android.R.color.darker_gray));
                }
                break;
        }
    }

    /**
     * the next button handler
     *
     * @param view the next button
     */
    public void goNext(View view) {
        Intent intent = new Intent(this, PaymentActivity3.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        finish();
    }

    /**
     * the previous button handler
     *
     * @param view the previous button
     */
    public void goPrevious(View view) {
        Intent intent = new Intent(this, PaymentActivity1.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();
    }

    /**
     * set the list items in the vertical step view
     * @param mStepview the vertical stepview
     * @param listItem list of items
     */
    private void setListItemForInstruction(VerticalStepView mStepview, List<String> listItem){

        mStepview.setStepsViewIndicatorComplectingPosition(listItem.size())
                .reverseDraw(false)//default is true
                .setTextSize(15)
                .setStepViewTexts(listItem)
                .setStepsViewIndicatorCompletedLineColor(getResources().getColor(R.color.colorAccent))
                .setStepsViewIndicatorUnCompletedLineColor(getResources().getColor(R.color.colorAccent))
                .setStepViewComplectedTextColor(getResources().getColor(R.color.colorAccent))
                .setStepViewUnComplectedTextColor(getResources().getColor(R.color.colorAccent))
                .setStepsViewIndicatorCompleteIcon(getResources().getDrawable(R.drawable.icon_check_round))
                .setStepsViewIndicatorDefaultIcon(getResources().getDrawable(R.drawable.icon_cancel_round))
                .setStepsViewIndicatorAttentionIcon(getResources().getDrawable(R.drawable.icon_replay));

    }
}
