package com.olafss.shamscorner.olafss_flashcard.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.view.activity.MainActivity;

public class GoHome {

    /** the class context */
    private Context context;

    public GoHome(Context context){
        this.context = context;
    }

    /**
     * The back button behavior class which will create a (yes - no) dialog
     */
    public void showDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(R.string.dialog_cancel_payment_text);
        alertDialogBuilder.setTitle(context.getResources().getString(R.string.dialog_cancel_payment));
        alertDialogBuilder.setPositiveButton(R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // if it is ok
                        Intent setIntent = new Intent(context, MainActivity.class);
                        context.startActivity(setIntent);
                        ((Activity)context).finish();
                    }
                });

        alertDialogBuilder.setNegativeButton(R.string.pay,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // if it is not ok
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
