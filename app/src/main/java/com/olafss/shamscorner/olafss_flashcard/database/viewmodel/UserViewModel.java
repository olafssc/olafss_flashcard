package com.olafss.shamscorner.olafss_flashcard.database.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.olafss.shamscorner.olafss_flashcard.database.data.NameAndLevel;
import com.olafss.shamscorner.olafss_flashcard.database.entity.UserData;
import com.olafss.shamscorner.olafss_flashcard.database.repository.UserDataRepository;

public class UserViewModel extends AndroidViewModel {

    /**
     * the repository for the user data
     */
    private UserDataRepository mUserDataRepository;

    /**
     * construct the view model for the user data
     *
     * @param application the main application object
     */
    public UserViewModel (Application application) {
        super(application);

        // get the user data repository
        mUserDataRepository = new UserDataRepository(application);
    }

    /**
     * get the all data of the user
     *
     * @return the user data
     */
    public UserData getUserData() {
        return mUserDataRepository.getUserData();
    }

    /**
     * get the name and level of the user
     *
     * @return the name and level as object
     */
    public LiveData<NameAndLevel> getNameAndLevel() {
        return mUserDataRepository.getNameAndLevel();
    }

    /**
     * delete all the data of the user
     */
    public void deleteUserData() {
        mUserDataRepository.deleteUserData();
    }

    /**
     * insert a row in the user table in the background
     *
     * @param userData the user data
     */
    public void insert(UserData userData) {
        mUserDataRepository.insert(userData);
    }

    /**
     * get the user id
     *
     * @return the user id
     */
    public String getUserId(){
        return mUserDataRepository.getUserId();
    }

    /**
     * get the user level as live data
     *
     * @return the user level
     */
    public LiveData<Integer> getUserLevel() {
        return mUserDataRepository.getUserLevel();
    }
}
