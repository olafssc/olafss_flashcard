package com.olafss.shamscorner.olafss_flashcard.database.data;

import android.arch.persistence.room.ColumnInfo;

public class CountData {
    @ColumnInfo(name = "i_know")
    public int iKnowCount;

    @ColumnInfo(name = "i_dont_know")
    public int iDontKnowCount;

    @ColumnInfo(name = "skipped")
    public int skippedCount;

    @ColumnInfo(name = "flipped")
    public int flippedCount;
}
