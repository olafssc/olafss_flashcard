package com.olafss.shamscorner.olafss_flashcard.view.controller;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.data.NewsFeedData;

import java.util.List;

public class ListItemRecycleNewsFeed extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * the news feed list
     */
    private List<NewsFeedData> newsFeedDataList;


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ListItemRecycleNewsFeed.NewsFeedViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_group, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        // get the main holder as news feed holder
        NewsFeedViewHolder holderThis = (NewsFeedViewHolder) holder;

        // get the news feed data from the list of data
        NewsFeedData newsFeedData = newsFeedDataList.get(position);

        // set the text into the news view from the news feed data
        holderThis.textView.setText(newsFeedData.text);
        // set the image icon in the individual icon section
        holderThis.imageView.setImageResource(newsFeedData.imageId);
    }

    @Override
    public int getItemCount() {
        return newsFeedDataList.size();
    }

    class NewsFeedViewHolder extends RecyclerView.ViewHolder {

        /**
         * text view for showing the news feed information
         */
        TextView textView;

        /**
         * image view for the individual icon
         */
        ImageView imageView;

        NewsFeedViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.tv_news);
            imageView = itemView.findViewById(R.id.img_icon);
        }
    }

    /**
     * set the recycler view items into the panel
     *
     * @param newsFeedDataList the list of the news feed in the group section
     */
    public void setItems(List<NewsFeedData> newsFeedDataList) {
        this.newsFeedDataList = newsFeedDataList;
    }
}
