package com.olafss.shamscorner.olafss_flashcard.view.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.utilities.ImageUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class ProfileEditDialogFragment extends DialogFragment implements View.OnClickListener {

    public static String TAG = "FullScreenDialogForProfile";

    /**
     * request code for taking the photo
     */
    private static final int REQUEST_TAKE_PHOTO = 1;

    /**
     * camera request code
     */
    private static final int MY_CAMERA_REQUEST_CODE = 100;

    /**
     * image view to show the picture that was taken
     */
    private ImageView imgTakePhoto;

    /**
     * the image file name
     */
    private String imageFileName = "";

    /**
     * the photo path of the user profile picture
     */
    private String mCurrentPhotoPath;

    /**
     * the image utility object
     */
    private ImageUtils imageUtils;

    /**
     * the image file converted to string
     */
    private String imageString;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // inflate the layout
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        // set the toolbar
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.icon_clear);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        toolbar.setTitle("Edit Profile");

        // get the data holder views
        imgTakePhoto = view.findViewById(R.id.img_profile);

        // set and add the on click listener for each image button
        // the firstname and lastname change button
        ImageView imgbtName = view.findViewById(R.id.imgbt_name);
        imgbtName.setOnClickListener(this);
        // the user name change button
        ImageView imgbtUsername = view.findViewById(R.id.imgbt_username);
        imgbtUsername.setOnClickListener(this);
        // the gender change button
        ImageView imgbtGender = view.findViewById(R.id.imgbt_gender);
        imgbtGender.setOnClickListener(this);
        // the date of birth change button
        ImageView imgbtDateOfBirth = view.findViewById(R.id.imgbt_dateofbirth);
        imgbtDateOfBirth.setOnClickListener(this);
        // the user address change button
        ImageView imgbtAddress = view.findViewById(R.id.imgbt_address);
        imgbtAddress.setOnClickListener(this);
        // the email address change button
        ImageView imgbtEmail = view.findViewById(R.id.imgbt_email);
        imgbtEmail.setOnClickListener(this);
        // the phone number change button
        ImageView imgbtPhone = view.findViewById(R.id.imgbt_phonenumber);
        imgbtPhone.setOnClickListener(this);
        // the camera upload photo button
        ImageButton imgbtCamera = view.findViewById(R.id.imgbt_camera);
        imgbtCamera.setOnClickListener(this);

        // the upgrade button
        Button btnUpgrade = view.findViewById(R.id.btn_upgrade);
        btnUpgrade.setOnClickListener(this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {

        // get the fragment manager
        FragmentManager fragmentManager = getFragmentManager();

        if (fragmentManager != null) {
            // the edit dialog
            EditDialog edtDialog;

            switch (v.getId()) {
                case R.id.imgbt_name:
                    // show the dialog for the name
                    edtDialog = new EditDialog();
                    edtDialog.setWhether2(1);
                    edtDialog.setDialogTitle("Name");
                    edtDialog.show(fragmentManager, "name_dialog");
                    break;

                case R.id.imgbt_username:
                    // show the dialog for the username
                    edtDialog = new EditDialog();
                    edtDialog.setWhether2(2);
                    edtDialog.setDialogTitle("Username");
                    edtDialog.show(fragmentManager, "username_dialog");
                    break;

                case R.id.imgbt_gender:
                    // show the dialog for the gender
                    edtDialog = new EditDialog();
                    edtDialog.setWhether2(3);
                    edtDialog.setDialogTitle("Gender");
                    edtDialog.show(fragmentManager, "gender_dialog");
                    break;

                case R.id.imgbt_dateofbirth:
                    // show the dialog picker for the date of birth
                    DatePickerFragmentProfile datePickerFragmentProfile = new DatePickerFragmentProfile();
                    // finally show the fragment dialog for choosing the date of birth
                    datePickerFragmentProfile.show(fragmentManager, "datePickerProfile");
                    break;

                case R.id.imgbt_address:
                    // show the dialog for the user location
                    edtDialog = new EditDialog();
                    edtDialog.setWhether2(2);
                    edtDialog.setDialogTitle("Address");
                    edtDialog.show(fragmentManager, "gender_address");
                    break;

                case R.id.imgbt_email:
                    // show the dialog for the email address
                    edtDialog = new EditDialog();
                    edtDialog.setWhether2(2);
                    edtDialog.setDialogTitle("Email");
                    edtDialog.show(fragmentManager, "gender_email");
                    break;

                case R.id.imgbt_phonenumber:
                    // show the dialog for the phone number
                    edtDialog = new EditDialog();
                    edtDialog.setWhether2(2);
                    edtDialog.setDialogTitle("Phone Number");
                    edtDialog.show(fragmentManager, "gender_phone");
                    break;

                case R.id.imgbt_camera:
                    // show the camera and handle the user photo
                    // take the camera permission at runtime
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (Objects.requireNonNull(getContext()).checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_CAMERA_REQUEST_CODE);
                        }
                    }
                    break;

                case R.id.btn_upgrade:
                    // show the upgrade dialog
                    new BuyNowDialog().show(fragmentManager, "dialog_buy");
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                Toast.makeText(getContext(), "Camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {

            // make the bitmap file square
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);

            // the image utils object
            imageUtils = new ImageUtils(getContext());

            // crop the image file
            Bitmap dstBmp = imageUtils.cropToSquareImage(bitmap);

            // set the image bitmap to the image view
            imgTakePhoto.setImageBitmap(dstBmp);

            // save the image file
            imageUtils.saveImage(dstBmp, mCurrentPhotoPath);

            // encode the image bitmap to string
            imageString = imageUtils.getStringImage(dstBmp);
            Log.i(TAG, "Image to String - " + imageString);
        }
    }

    /**
     * dispatch the image file from the given authority
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();

                // show a warning message
                Toast.makeText(getContext(), R.string.something_wrong, Toast.LENGTH_LONG).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        "com.olafss.shamscorner.olafss_flashcard.fileProvider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    /**
     * create the image file from the intent result
     *
     * @return the image file
     * @throws IOException input output exception with no image data found
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "JPEG_" + timeStamp;
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // add the .jpg extension
        imageFileName += ".jpg";

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /**
     * dialog class for the edit option for each items
     */
    public static class EditDialog extends DialogFragment {

        /**
         * whether 2 text field or 1 (yes - 2 text field)
         */
        private int whether2;

        /**
         * the title for the dialog
         */
        private String dialogTitle;

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(dialogTitle);
            // get the layout inflater
            LayoutInflater inflater = getActivity().getLayoutInflater();

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout

            if (whether2 == 1) {
                builder.setView(inflater.inflate(R.layout.dialog_edit_profile_item, null))
                        // Add action buttons
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                // update the text in the database
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getDialog().cancel();
                            }
                        });
            } else if (whether2 == 2) {
                builder.setView(inflater.inflate(R.layout.dialog_edit_profile_item_single, null))
                        // Add action buttons
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                // update the text in the database
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getDialog().cancel();
                            }
                        });
            } else if (whether2 == 3) {
                builder.setItems(R.array.array_gender, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                    }
                });
            }

            // finally create the dialog fragment
            return builder.create();
        }

        /**
         * the type of layout
         *
         * @param b any number
         */
        public void setWhether2(int b) {
            whether2 = b;
        }

        /**
         * set the title for this dialog
         *
         * @param dialogTitle title text
         */
        public void setDialogTitle(String dialogTitle) {
            this.dialogTitle = dialogTitle;
        }
    }

    /**
     * the date picker for the user date of birth
     */
    public static class DatePickerFragmentProfile extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        public DatePickerFragmentProfile() {
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            String dateOfBirth = day + "-" + month + "-" + year;
            //Log.i("DATE_OF_BIRTH", dateOfBirth);
            // todo: update the data both into the server and the phone
        }
    }
}
