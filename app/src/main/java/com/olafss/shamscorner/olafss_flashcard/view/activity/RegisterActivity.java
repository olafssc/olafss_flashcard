package com.olafss.shamscorner.olafss_flashcard.view.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.config.AppConfig;
import com.olafss.shamscorner.olafss_flashcard.config.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    /**
     * main tag for the register of the user
     */
    private static final String TAG = RegisterActivity.class.getSimpleName();

    /**
     * gender of the user (male-0, female-1, others-2)
     */
    private int gender = 3; // 3 means, the user forgot to select the gender

    /**
     * the date of birth picker button
     */
    private Button btnDateOfBirth;

    /**
     * user date of birth as day, month, year
     */
    private static String dateOfBirth = null;

    //**
    // * view model for the user table
    // */
    //private UserViewModel mUserViewModel;

    /**
     * the progressbar for registering
     */
    private ProgressBar progressBar;

    /** the username input field */
    private EditText edtUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // check and set the gender attribute
        if (((RadioButton) findViewById(R.id.radio_male)).isChecked()) {
            gender = 0;
        } else if (((RadioButton) findViewById(R.id.radio_female)).isChecked()) {
            gender = 1;
        } else if (((RadioButton) findViewById(R.id.radio_others)).isChecked()) {
            gender = 2;
        }

        // user date of birth selection dialog setup
        btnDateOfBirth = findViewById(R.id.btn_date_of_birth);
        // set the action listener
        btnDateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create object
                DatePickerFragment datePickerFragment = new DatePickerFragment();
                // set the button into the class
                datePickerFragment.setArgs(btnDateOfBirth);
                // finally show the fragment dialog for choosing the date of birth
                datePickerFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

        // initiate the database helper object for the user table
        //mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        // initiate the progress bar
        progressBar = findViewById(R.id.register_progress);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.color_teal),
                android.graphics.PorterDuff.Mode.MULTIPLY);

        // the username input field
        edtUsername = findViewById(R.id.edt_username);

        // check for the username when the user out of focus from the username input field
        edtUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String s = edtUsername.getText().toString().trim();
                if (!hasFocus && !s.isEmpty()) {
                    // not in focus
                    checkUsernameAvailable(v, s);
                }
            }
        });
    }

    /**
     * button click handler for the radio group for the gender
     *
     * @param view the radio button
     */
    public void onRadioButtonClickedGender(View view) {

        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_male:
                if (checked)
                    // the user is male
                    gender = 0;
                break;
            case R.id.radio_female:
                if (checked)
                    // the user is female
                    gender = 1;
                break;
            case R.id.radio_others:
                if (checked)
                    // the user is others
                    gender = 2;
                break;
        }
    }

    /**
     * button click handler for the register the user
     *
     * @param view the clicked view
     */
    public void onButtonClickedRegister(View view) {

        // first hide the keyboard
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager != null) {
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }

        // check for the required attribute
        // the gender group
        RadioGroup radioGroupGender = findViewById(R.id.radio_group_gender);
        if (gender == 3) {
            radioGroupGender.setBackgroundColor(getResources().getColor(R.color.color_red_dark));
        } else {
            radioGroupGender.setBackgroundColor(getResources().getColor(R.color.bgCard));
        }

        // the firstname edit text field
        EditText edtFirstname = findViewById(R.id.edt_firstname);
        // get the first name from the input field
        String firstname = edtFirstname.getText().toString().trim();
        // check if it is empty or not
        errorCheck(firstname, edtFirstname);

        // check for the date of birth attribute
        errorCheck(dateOfBirth, btnDateOfBirth);

        // get the username from the input field
        String username = edtUsername.getText().toString().trim();
        // check if it is empty or not
        errorCheck(username, edtUsername);

        // the password edit text field
        EditText edtPassword = findViewById(R.id.edt_password);
        // get the password from the input field
        String password = edtPassword.getText().toString().trim();
        // check if it is empty or not
        errorCheck(password, edtPassword);

        // get the lastname
        EditText edtLastname = findViewById(R.id.edt_lastname);
        String lastname = edtLastname.getText().toString().trim();

        // get the address
        EditText edtAddress = findViewById(R.id.edt_address);
        String address = edtAddress.getText().toString().trim();

        // get the email
        EditText edtEmail = findViewById(R.id.edt_email);
        String email = edtEmail.getText().toString().trim();

        // get the phone
        EditText edtPhone = findViewById(R.id.edt_phone);
        String phone = edtPhone.getText().toString().trim();

        // check and register
        if (!firstname.isEmpty() && gender != 3 && !dateOfBirth.isEmpty() && !username.isEmpty() &&
                !password.isEmpty()) {
            registerUser(view, firstname, lastname, gender, dateOfBirth, address,
                    username, password, email, phone);
        } else {
            Toast.makeText(this, getResources().getString(R.string.enter_details),
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * check the username is available or not when the input field is out of focus
     *
     * @param view the username input field
     */
    private void checkUsernameAvailable(final View view, final String username) {

        // Tag used to cancel the request
        String tag_string_req = "req_for_username";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_USERNAME_VALIDATION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("USERNAME_VALIDATE", "Username validation Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // the username is not available
                        view.setBackgroundColor(getResources().getColor(R.color.bgCard));
                    } else {
                        // the username is available
                        view.setBackgroundColor(getResources().getColor(R.color.color_red_dark));
                        // show a message
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.username_available),
                                Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("USERNAME_VALIDATE", "Username validation Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_connection),
                        Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<>();
                params.put("username", username);

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * register the user in the server
     *
     * @param view        the register button
     * @param firstname   user firstname
     * @param lastname    user lastname
     * @param gender      user gender
     * @param dateOfBirth user date of birth
     * @param address     user address
     * @param username    user username
     * @param password    user password
     */
    private void registerUser(final View view, final String firstname, final String lastname, final int gender,
                              final String dateOfBirth, final String address, final String username,
                              final String password, final String email, final String phone) {

        // Tag used to cancel the request
        String tag_string_req = "req_register";

        // show the progressbar
        showProgress(view);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // User successfully stored in MySQL
                        Toast.makeText(getApplicationContext(), R.string.registered_login_now, Toast.LENGTH_LONG).show();

                        // Launch login activity
                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        finish();
                    } else {
                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();

                        // hide the progressbar
                        hideProgress(view);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    // hide the progressbar
                    hideProgress(view);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_connection),
                        Toast.LENGTH_LONG).show();

                // hide the progressbar
                hideProgress(view);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<>();
                params.put("name", firstname + " " + lastname);
                params.put("gender", String.valueOf(gender));
                params.put("date_of_birth", dateOfBirth);
                params.put("username", username);
                params.put("password", password);
                params.put("address", address);
                params.put("email", email);
                params.put("phone", phone);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * show the progress bar and hide the button
     *
     * @param view the clicked button
     */
    private void showProgress(View view) {
        // hide the login button
        view.setVisibility(View.GONE);
        // show the progress bar
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * show the progress bar and hide the button
     *
     * @param view the clicked button
     */
    private void hideProgress(View view) {
        // show the login button
        view.setVisibility(View.VISIBLE);
        // hide the progress bar
        progressBar.setVisibility(View.GONE);
    }

    /**
     * check whether the user input something in the input field or not
     *
     * @param text the user text input
     * @param view the view which the user input the information
     */
    private void errorCheck(String text, View view) {
        if (TextUtils.isEmpty(text)) {
            view.setBackgroundColor(getResources().getColor(R.color.color_red_dark));
        } else {
            view.setBackgroundColor(getResources().getColor(R.color.bgCard));
        }
    }

    /**
     * go to the login activity if have an account
     *
     * @param view clicked view
     */
    public void onClickLogin(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    /**
     * the date picker for the user date of birth
     */
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private Button btnDateOfBirth;

        public DatePickerFragment(){}

        public void setArgs(Button btnDateOfBirth){
            this.btnDateOfBirth = btnDateOfBirth;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            dateOfBirth = day + "-" + month + "-" + year;
            //Log.i("DATE_OF_BIRTH", dateOfBirth);

            // set the data into the date of birth button as text
            btnDateOfBirth.setText(dateOfBirth);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();
    }
}
