package com.olafss.shamscorner.olafss_flashcard.view.activity;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.DownloadManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.config.AppConfig;
import com.olafss.shamscorner.olafss_flashcard.config.AppController;
import com.olafss.shamscorner.olafss_flashcard.database.entity.UserData;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.QuestionViewModel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.UserViewModel;
import com.olafss.shamscorner.olafss_flashcard.utilities.DownloadUtils;
import com.olafss.shamscorner.olafss_flashcard.utilities.SessionManager;
import com.olafss.shamscorner.olafss_flashcard.view.controller.DoThingsForLogin;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    /**
     * the main tag for logging
     */
    private static final String TAG = LoginActivity.class.getSimpleName();

    /**
     * the login session manager
     */
    private SessionManager session;

    /**
     * the progressbar for loading
     */
    private ProgressBar progressBar;

    /**
     * the user view model
     */
    private UserViewModel mUserViewModel;

    /**
     * the question view model
     */
    private QuestionViewModel mQuestionViewModel;

    /**
     * is first time launch or not ( 0 - no, 1 - yes)
     */
    private int isFirstTimeLaunch;

    /**
     * edit text for the username and the password
     */
    private EditText edtUsername;
    private EditText edtPassword;

    /**
     * the download manger for downloading the necessary data from the server
     */
    private DownloadManager downloadManager;

    /** the do things for the login object */
    private DoThingsForLogin doThingsForLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        // get the session object
        session = new SessionManager(getApplicationContext());

        // check if it is first time launch or not
        if (session.isFirstTimeLaunch()) {
            isFirstTimeLaunch = 1;

            Log.i("IS_FIRST_TIME_LAUNCH", "yes");
        } else {
            isFirstTimeLaunch = 2;
            Log.i("IS_FIRST_TIME_LAUNCH", "no");
        }

        // intent for the broadcast receiver
        String downloadCompleteIntentName = DownloadManager.ACTION_DOWNLOAD_COMPLETE;
        IntentFilter downloadCompleteIntentFilter = new IntentFilter(downloadCompleteIntentName);

        // register the receiver
        registerReceiver(downloadCompleteReceiver, downloadCompleteIntentFilter);

        // get the download manager from android system
        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

        // the username edit text field
        edtUsername = findViewById(R.id.edt_username);
        edtUsername.setOnFocusChangeListener(this);
        // the password edit text field
        edtPassword = findViewById(R.id.edt_password);
        edtPassword.setOnFocusChangeListener(this);

        // initiate the view model for the user table
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        // initiate the question model for the question table
        mQuestionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);

        // initiate the progress bar
        progressBar = findViewById(R.id.login_progress);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.color_teal),
                android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    /**
     * the login button action, the user will be logged in if valid
     *
     * @param view the clicked view
     */
    public void onButtonClickLogin(View view) {

        // first hide the keyboard
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager != null) {
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }

        // remove the focus in the input field and make the layout center
        edtPassword.clearFocus();
        edtUsername.clearFocus();

        // get the username from the input field
        String username = edtUsername.getText().toString().trim();
        // check if it is empty or not
        errorCheck(username, edtUsername);

        // get the password from the input field
        String password = edtPassword.getText().toString().trim();
        // check if it is empty or not
        errorCheck(password, edtPassword);

        // check then proceed to login
        if (!username.isEmpty() && !password.isEmpty()) {
            // then proceed to login

            // initiate the do things for login object
            doThingsForLogin = new DoThingsForLogin(LoginActivity.this, mQuestionViewModel);

            checkLogin(username, password, view);
        } else {
            // Prompt user to enter credentials
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.required_input_error),
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * method to verify login details in mysql database
     *
     * @param username the user username
     * @param password the user password
     */
    private void checkLogin(final String username, final String password, final View view) {

        // Tag used to cancel the request
        String tag_string_req = "req_login";

        // show the progressbar
        showProgress(view);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // get the user json object
                        JSONObject user = jObj.getJSONObject("user");

                        // first delete the current user
                        mUserViewModel.deleteUserData();

                        // Inserting row in users table todo: add the unique id
                        mUserViewModel.insert(new UserData(
                                user.getString("username"),
                                user.getString("name"),
                                user.getString("email"),
                                user.getString("phone"),
                                Integer.parseInt(user.getString("gender")),
                                user.getString("date_of_birth"),
                                user.getString("image_url"),
                                user.getString("address"),
                                Integer.parseInt(user.getString("level")),
                                Integer.parseInt(user.getString("paid"))));

                        // sync and add the question table from the server
                        doThingsForLogin.syncQuestions(username, isFirstTimeLaunch, downloadManager, view, progressBar);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();

                        // hide the progressbar
                        hideProgress(view);
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.something_wrong),
                            Toast.LENGTH_LONG).show();

                    // hide the progressbar
                    hideProgress(view);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_connection),
                        Toast.LENGTH_LONG).show();

                // hide the progressbar
                hideProgress(view);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                params.put("sub_code", AppConfig.APP_SUBJECT_CODE);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * show the progress bar and hide the button
     *
     * @param view the clicked button
     */
    private void showProgress(View view) {
        // hide the login button
        view.setVisibility(View.INVISIBLE);
        // show the progress bar
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * show the progress bar and hide the button
     *
     * @param view the clicked button
     */
    private void hideProgress(View view) {
        // show the login button
        view.setVisibility(View.VISIBLE);
        // hide the progress bar
        progressBar.setVisibility(View.INVISIBLE);
    }


    /**
     * check whether the user input something in the input field or not
     *
     * @param text the user text input
     * @param view the view which the user input the information
     */
    private void errorCheck(String text, View view) {
        if (TextUtils.isEmpty(text)) {
            view.setBackgroundColor(getResources().getColor(R.color.color_red_dark));
        } else {
            view.setBackgroundColor(getResources().getColor(R.color.bgCard));
        }
    }

    /**
     * click event for go the register activity
     *
     * @param view clicked view
     */
    public void onClickRegister(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();
    }

    /**
     * click event for the forgot password button
     *
     * @param view the clicked view
     */
    public void onClickForgotPassword(View view) {
        // TODO: implement the forgot password functionality

        // 1. redirect to the username input dialog
        // 2. if the username available then got to 3 otherwise go to 6
        // 3. if the email available then go to 4 otherwise go to 5
        // 4. send a dummy password to the email address
        // 5. redirect to the contact page
        // 6. show username is not available
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(hasFocus){
            View view = findViewById(R.id.container_login);
            ObjectAnimator animation;
            animation = ObjectAnimator.ofFloat(view, "translationY", -260f);
            animation.setDuration(500);
            animation.start();
        }
    }

    private BroadcastReceiver downloadCompleteReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            long downloadZipId = doThingsForLogin.getDownloadZipId();

            if (referenceId == downloadZipId) {

                Log.i(TAG, "files downloaded");

                String path;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    path = Objects.requireNonNull(getExternalFilesDir(Environment.getRootDirectory().getPath()))
                            .getPath() + "/";
                } else {
                    path = "/storage/emulated/0/Android/data/com.olafss.shamscorner.olafss_flashcard/files/system/";
                }

                // then unzip and delete the downloaded files
                new DownloadUtils().deleteAndUnzip(path, AppConfig.FILE_NAME_ZIP,
                        downloadManager, downloadZipId, AppConfig.IMAGE_LOCATION_DIRECTORY);

            } else {
                Log.d(TAG, "id not matched");
            }

            // Launch main activity
            Intent launcherIntent = new Intent(context, LauncherActivity.class);
            startActivity(launcherIntent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            finish();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // stop receiving the response for the broadcast receiver
        unregisterReceiver(downloadCompleteReceiver);
    }
}
