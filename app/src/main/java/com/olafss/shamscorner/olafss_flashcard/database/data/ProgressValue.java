package com.olafss.shamscorner.olafss_flashcard.database.data;

import android.arch.persistence.room.ColumnInfo;

public class ProgressValue {

    @ColumnInfo(name = "new")
    public int newVal;

    @ColumnInfo(name = "learning")
    public int learningVal;

    @ColumnInfo(name = "review")
    public int reviewVal;

    @ColumnInfo(name = "master")
    public int masterVal;
}
