package com.olafss.shamscorner.olafss_flashcard.view.activity.payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.baoyachi.stepview.HorizontalStepView;
import com.baoyachi.stepview.bean.StepBean;
import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.utilities.GoHome;
import com.olafss.shamscorner.olafss_flashcard.view.activity.LauncherActivity;

import java.util.ArrayList;
import java.util.List;

public class PaymentActivity1 extends AppCompatActivity {

    /**
     * the payment option
     */
    private String option;

    /**
     * the key id of the selected option
     */
    private static final String SELECTED_OPTION = "SELECTED_OPTION";

    /**
     * the next button
     */
    private Button btnNext;

    /**
     * the option radio buttons
     */
    private RadioButton radioBkash, radioRocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_step_1);

        // set the steps view
        HorizontalStepView setpview5 = findViewById(R.id.step_view);
        List<StepBean> stepsBeanList = new ArrayList<>();
        StepBean stepBean0 = new StepBean("Choose",-1);
        StepBean stepBean1 = new StepBean("Pay",-1);
        StepBean stepBean2 = new StepBean("Enter",-1);
        StepBean stepBean3 = new StepBean("Go",-1);
        stepsBeanList.add(stepBean0);
        stepsBeanList.add(stepBean1);
        stepsBeanList.add(stepBean2);
        stepsBeanList.add(stepBean3);
        setpview5
                .setStepViewTexts(stepsBeanList)
                .setTextSize(12)//set textSize
                .setStepsViewIndicatorCompletedLineColor(getResources().getColor(R.color.colorAccent))
                .setStepsViewIndicatorUnCompletedLineColor(getResources().getColor(R.color.colorAccent))
                .setStepViewComplectedTextColor(getResources().getColor(R.color.colorAccent))
                .setStepViewUnComplectedTextColor(getResources().getColor(R.color.colorAccent))
                .setStepsViewIndicatorCompleteIcon(getResources().getDrawable(R.drawable.icon_check_round))
                .setStepsViewIndicatorDefaultIcon(getResources().getDrawable(R.drawable.icon_cancel_round))
                .setStepsViewIndicatorAttentionIcon(getResources().getDrawable(R.drawable.icon_replay));

        // make enabled the next button until an option selected
        btnNext = findViewById(R.id.btn_next);
        btnNext.setEnabled(false);
        btnNext.setTextColor(getResources().getColor(android.R.color.darker_gray));

        // initiate the radio buttons
        radioBkash = findViewById(R.id.radio_bkash);
        radioRocket = findViewById(R.id.radio_rocket);
    }

    @Override
    public void onBackPressed() {
        new GoHome(this).showDialog();
    }

    /**
     * go to the payment activity 2
     *
     * @param view the clicked view
     */
    public void goNext(View view) {
        Intent intent = new Intent(this, PaymentActivity2.class);
        intent.putExtra(SELECTED_OPTION, option);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        finish();
    }

    /**
     * cancel the payment and go to the home page
     *
     * @param view the clicked view
     */
    public void goCancel(View view) {
        Intent intent = new Intent(this, LauncherActivity.class);
        startActivity(intent);
    }

    /**
     * when user click on the bkash option
     * @param view the clicked view
     */
    public void clickBkash(View view) {
        // check the bkash radio button
        radioBkash.setChecked(true);
        // uncheck the rocket radio button
        radioRocket.setChecked(false);

        // make the next button enabled
        btnNext.setEnabled(true);
        btnNext.setTextColor(getResources().getColor(R.color.colorAccent));

        option = "bkash";
    }

    /**
     * when user click on the rocket option
     * @param view the clicked view
     */
    public void clickRocket(View view) {
        // uncheck the bkash radio button
        radioBkash.setChecked(false);
        // check the rocket radio button
        radioRocket.setChecked(true);

        // make the next button enabled
        btnNext.setEnabled(true);
        btnNext.setTextColor(getResources().getColor(R.color.colorAccent));

        option = "rocket";
    }
}
