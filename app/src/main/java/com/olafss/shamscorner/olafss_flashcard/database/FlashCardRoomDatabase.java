package com.olafss.shamscorner.olafss_flashcard.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.olafss.shamscorner.olafss_flashcard.database.dao.ProgressDataDao;
import com.olafss.shamscorner.olafss_flashcard.database.dao.QuestionDataDao;
import com.olafss.shamscorner.olafss_flashcard.database.dao.UserDataDao;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ChapterData;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ImageData;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ProgressData;
import com.olafss.shamscorner.olafss_flashcard.database.entity.QuestionData;
import com.olafss.shamscorner.olafss_flashcard.database.entity.UserData;

import java.util.ArrayList;
import java.util.List;

@Database(
        entities = {
                UserData.class,
                QuestionData.class,
                ProgressData.class,
                ChapterData.class,
                ImageData.class
        },
        version = 4
)
public abstract class FlashCardRoomDatabase extends RoomDatabase {

    /** the user data dao */
    public abstract UserDataDao userDataDao();

    /** the questions data dao */
    public abstract QuestionDataDao questionDataDao();

    /** the progress data dao */
    public abstract ProgressDataDao progressDataDao();

    /** the single ton instance */
    private static FlashCardRoomDatabase INSTANCE;

    /** single ton method to get the database */
    public static FlashCardRoomDatabase getDatabase(final Context context){
        if (INSTANCE == null) {
            synchronized (FlashCardRoomDatabase.class) {
                if (INSTANCE == null) {
                    // create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            FlashCardRoomDatabase.class, "olafss_flashcard_db")
                            .fallbackToDestructiveMigration()
                            //.addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }



    /*/**
     * delete all content and repopulate the database whenever the app is started
     *//*
    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final UserDataDao userDataDao;
        private final ProgressDataDao progressDataDao;

        PopulateDbAsync(FlashCardRoomDatabase db) {
            userDataDao = db.userDataDao();
            progressDataDao = db.progressDataDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            userDataDao.deleteUserData();
            UserData userData = new UserData("shamscorner", "MD. Kanak Hossain",
                    "hossains159@gmail.com", "01733412606", 0,
                    "01-01-1994", "something",
                    "Mohammadpur, Dhaka", 0, 0);
            userDataDao.insertUser(userData);

            progressDataDao.deleteProgressData();

            List<ProgressData> progressDataList = new ArrayList<>();

            progressDataList.add(new ProgressData(1, 1, "Chapter - " + 1,
                    20, 30, 10, 46));

            progressDataList.add(new ProgressData(1, 2, "Chapter - " + 2,
                    20, 30, 10, 46));

            progressDataList.add(new ProgressData(1, 3, "Chapter - " + 3,
                    20, 56, 10, 85));

            progressDataList.add(new ProgressData(1, 4, "Chapter - " + 4,
                    20, 30, 17, 46));

            progressDataList.add(new ProgressData(1, 5, "Chapter - " + 5,
                    20, 34, 10, 46));

            progressDataList.add(new ProgressData(2, 1, "Chapter - " + 1,
                    20, 30, 10, 53));

            progressDataList.add(new ProgressData(2, 2, "Chapter - " + 2,
                    20, 89, 86, 46));

            progressDataList.add(new ProgressData(2, 3, "Chapter - " + 3,
                    20, 30, 10, 57));

            progressDataList.add(new ProgressData(2, 4, "Chapter - " + 4,
                    20, 56, 10, 46));

            progressDataList.add(new ProgressData(2, 5, "Chapter - " + 5,
                    20, 30, 78, 46));

            progressDataDao.insertProgressList(progressDataList);

            return null;
        }
    }*/
}
