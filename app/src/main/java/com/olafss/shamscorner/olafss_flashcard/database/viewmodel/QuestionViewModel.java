package com.olafss.shamscorner.olafss_flashcard.database.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.olafss.shamscorner.olafss_flashcard.database.data.ChapterNameAndNumber;
import com.olafss.shamscorner.olafss_flashcard.database.data.ChapterNumberAndPart;
import com.olafss.shamscorner.olafss_flashcard.database.data.CountData;
import com.olafss.shamscorner.olafss_flashcard.database.data.ImageValue;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ChapterData;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ImageData;
import com.olafss.shamscorner.olafss_flashcard.database.entity.QuestionData;
import com.olafss.shamscorner.olafss_flashcard.database.repository.QuestionDataRepository;

import org.json.JSONArray;

import java.util.List;

public class QuestionViewModel extends AndroidViewModel {

    /**
     * the repository for the question data
     */
    private QuestionDataRepository mQuestionDataRepository;

    /**
     * construct the view model for the question data
     *
     * @param application the main application object
     */
    public QuestionViewModel(Application application) {
        super(application);

        // get the question data repository
        mQuestionDataRepository = new QuestionDataRepository(application);
    }

    /**
     * get the chapter name and number as a list
     *
     * @param part the subject part
     * @return list of chapter name and number
     */
    public List<ChapterNameAndNumber> getChapterNameAndNumberList(int part) {
        return mQuestionDataRepository.getChapterNameAndNumberList(part);
    }

    /**
     * get the number of questions with the specified chapter, part and state
     *
     * @param part    the subject part
     * @param chapter the chapter number
     * @param state   the current state (0-new, 1-learning, 2-review, 3-master)
     * @return the number of count of the questions
     */
    public int getProgressState(int part, int chapter, int state) {
        return mQuestionDataRepository.getProgressState(part, chapter, state);
    }

    /**
     * get the total number of questions in the given chapter and part
     *
     * @param part    the subject part
     * @param chapter the chapter number
     * @return the total number of questions
     */
    public int getTotalQuestionCount(int part, int chapter) {
        return mQuestionDataRepository.getTotalQuestionCount(part, chapter);
    }

    /**
     * update the questions state in the given question
     *
     * @param stateList the list of states
     */
    public void updateStateList(JSONArray stateList) {
        mQuestionDataRepository.updateStateList(stateList);
    }

    /**
     * update the question paid column according to the question unique id
     *
     * @param paidList the list of question id and the paid column
     * @param isPaid the paid or not - boolean
     */
    public void updateIdAndPaidQuestions(JSONArray paidList, boolean isPaid) {
        mQuestionDataRepository.updateIdAndPaidQuestions(paidList, isPaid);
    }

    /**
     * delete the current chapter data
     */
    public void deleteChapterData() {
        mQuestionDataRepository.deleteChapterData();
    }

    /**
     * delete the current question data
     */
    public void deleteQuestionData() {
        mQuestionDataRepository.deleteQuestionData();
    }

    /**
     * delete the current image data
     */
    public void deleteImageData() {
        mQuestionDataRepository.deleteImageData();
    }

    /**
     * insert the chapter data into the chapter table
     */
    public void insertChapterList(JSONArray chapterDataList) {
        mQuestionDataRepository.insertChapterList(chapterDataList);
    }

    /**
     * insert the question data into the question table
     */
    public void insertQuestionList(JSONArray questionDataList, boolean isPaid) {
        mQuestionDataRepository.insertQuestionList(questionDataList, isPaid);
    }

    /**
     * insert the image data into the image table
     */
    public void insertImageList(JSONArray imageDataList) {
        mQuestionDataRepository.insertImageList(imageDataList);
    }

    /**
     * reset the current question states for the chapter and part
     *
     * @param chapter the subject chapter
     * @param part    the subject part
     */
    public void resetQuestionState(int chapter, int part) {
        mQuestionDataRepository.resetQuestionState(chapter, part);
    }

    /**
     * get the question data according to the given chapter and part
     *
     * @param chapter the chapter number
     * @param part    the subject part
     * @return the question data object
     */
    public QuestionData getQuestion(int chapter, int part) {
        return mQuestionDataRepository.getQuestion(chapter, part);
    }

    /**
     * update the state of the given question id
     * @param id the question id
     * @param val the value
     */
    public void updateState(int id, int val){
        mQuestionDataRepository.updateState(id, val);
    }

    /**
     * update the flipped counter of the given question id
     * @param id the question id
     * @param val the value
     */
    public void updateFlipped(int id, int val){
        mQuestionDataRepository.updateFlipped(id, val);
    }

    /**
     * update the skipped counter of the given question id
     * @param id the question id
     * @param val the value
     */
    public void updateSkipped(int id, int val){
        mQuestionDataRepository.updateSkipped(id, val);
    }

    /**
     * update the i know counter of the given question id
     * @param id the question id
     * @param val the value
     */
    public void updateIKnow(int id, int val){
        mQuestionDataRepository.updateiKnow(id, val);
    }

    /**
     * update the i don't know counter of the given question id
     * @param id the question id
     * @param val the value
     */
    public void updateIDontKnow(int id, int val){
        mQuestionDataRepository.updateIDontKnow(id, val);
    }

    /**
     * get the image data from the given id and type, front or back
     *
     * @param id      the question id
     * @param isFront front or back
     * @return image value (url, position)
     */
    public ImageValue getImageData(int id, int isFront) {
        return mQuestionDataRepository.getImageData(id, isFront);
    }

    /**
     * get the total number of count of count data object (i know, i don't know, skipped, flipped)
     *
     * @return the counter of each items in the count data object
     */
    public LiveData<CountData> getCountData(){
        return mQuestionDataRepository.getCountData();
    }

    /**
     * get the recommendation for the recommend panel
     *
     * @return list of chapters
     */
    public LiveData<List<ChapterNumberAndPart>> getRecommendations() {
        return mQuestionDataRepository.getRecommendations();
    }
}
