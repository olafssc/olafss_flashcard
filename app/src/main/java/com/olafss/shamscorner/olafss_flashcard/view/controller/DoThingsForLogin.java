package com.olafss.shamscorner.olafss_flashcard.view.controller;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.config.AppConfig;
import com.olafss.shamscorner.olafss_flashcard.config.AppController;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.QuestionViewModel;
import com.olafss.shamscorner.olafss_flashcard.utilities.DownloadUtils;
import com.olafss.shamscorner.olafss_flashcard.utilities.SessionManager;
import com.olafss.shamscorner.olafss_flashcard.view.activity.LauncherActivity;
import com.olafss.shamscorner.olafss_flashcard.view.activity.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DoThingsForLogin {

    /**
     * the main tag for logging
     */
    private static final String TAG = LoginActivity.class.getSimpleName();

    /**
     * the question view model
     */
    private QuestionViewModel mQuestionViewModel;

    /**
     * the application context
     */
    private Context context;

    /**
     * the download zip code
     */
    private long downloadZipId;

    /**
     * the download utils class
     */
    private DownloadUtils downloadUtils;

    /**
     * the session manager for the shared pref
     */
    private SessionManager session;

    /**
     * construct the do things in the background for the login activity
     *
     * @param context application context
     * @param mQuestionViewModel the question view model for the question table
     */
    public DoThingsForLogin(Context context, QuestionViewModel mQuestionViewModel) {

        // get the context
        this.context = context;
        // get the question view model
        this.mQuestionViewModel = mQuestionViewModel;

        // get the session object
        session = new SessionManager(context);

        // get the download utils object
        downloadUtils = new DownloadUtils();
    }

    /**
     * method to sync all the questions from the server
     *
     * @param username    the user username
     */
    public void syncQuestions(final String username, final int isFirstTimeLaunch,
                              final DownloadManager downloadManager, final View view, final ProgressBar progressBar) {

        final String subjectCode = AppConfig.APP_SUBJECT_CODE;

        // Tag used to cancel the request
        String tag_string_req = "req_sync_questions";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SYNC_QUESTIONS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Question Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // questions synced successfully

                        // check if the app launch for the first time or not
                        if (isFirstTimeLaunch == 1) {

                            // delete the current data from the question, chapter, image table
                            mQuestionViewModel.deleteQuestionData();
                            mQuestionViewModel.deleteChapterData();
                            mQuestionViewModel.deleteImageData();

                            // get the chapter list from the server and insert into the chapter table
                            mQuestionViewModel.insertChapterList(jObj.getJSONArray("chapters"));

                            // sync and add the question table from the server
                            mQuestionViewModel.insertQuestionList(jObj.getJSONArray("question"),
                                    jObj.getBoolean("is_paid"));

                            // get the image list from the server and put a image array in the json
                            mQuestionViewModel.insertImageList(jObj.getJSONArray("images"));

                            // first download the files
                            downloadZipId = downloadUtils.downloadZipFile(context, downloadManager);

                            // set the first time launch false
                            session.setFirstTimeLaunch(false);

                        } else {
                            mQuestionViewModel.updateIdAndPaidQuestions(
                                    jObj.getJSONArray("question"),
                                    jObj.getBoolean("is_paid")
                            );
                            // open launcher
                            Intent intent = new Intent(context, LauncherActivity.class);
                            context.startActivity(intent);
                            ((Activity) context).overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                            ((Activity) context).finish();
                        }

                    } else {
                        // something went wrong
                        Toast.makeText(context, context.getResources().getString(R.string.error_heppened),
                                Toast.LENGTH_LONG).show();

                        session.setLogin(false);

                        hideProgress(view, progressBar);
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(context, context.getString(R.string.something_wrong),
                            Toast.LENGTH_LONG).show();

                    session.setLogin(false);

                    hideProgress(view, progressBar);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Question Error: " + error.getMessage());
                Toast.makeText(context, context.getResources().getString(R.string.check_connection),
                        Toast.LENGTH_LONG).show();

                session.setLogin(false);

                hideProgress(view, progressBar);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("user_id", username);
                params.put("sub_code", subjectCode);
                params.put("is_first_time", String.valueOf(isFirstTimeLaunch));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * return the download zip id
     *
     * @return the download id of the zip file
     */
    public long getDownloadZipId() {
        return downloadZipId;
    }

    /**
     * show the progress bar and hide the button
     *
     * @param view the clicked button
     * @param progressBar the loading progress bar
     */
    private void hideProgress(View view, ProgressBar progressBar) {

        if(view != null) {
            // show the login button
            view.setVisibility(View.VISIBLE);
        }
        // hide the progress bar
        progressBar.setVisibility(View.INVISIBLE);
    }
}
