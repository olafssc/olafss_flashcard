package com.olafss.shamscorner.olafss_flashcard.view.fragment;


import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.config.AppConfig;
import com.olafss.shamscorner.olafss_flashcard.database.data.ImageValue;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.QuestionViewModel;

import java.io.File;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionPanelFragment extends Fragment {

    /**
     * the id of the question state
     */
    private static final String QUESTION_STATE = "question_state";

    /**
     * the id of the question state
     */
    private static final String QUESTION_NAME = "question_name";

    /**
     * the id of the question unique id
     */
    private static final String QUESTION_ID = "question_un_id";

    /**
     * the id of the card back or front
     */
    private static final String QUESTION_IS_FRONT = "is_front";

    /**
     * the state of the question
     */
    private int mState;

    /**
     * the text on the panel
     */
    private String mName;

    /**
     * the question unique id
     */
    private int id;

    /**
     * whether front or back, true-front, false-back
     */
    private int isFront;

    /**
     * the main path of the image url
     */
    private String mainPath;

    public QuestionPanelFragment() {
        // Required empty public constructor
    }


    @SuppressLint("StaticFieldLeak")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            id = savedInstanceState.getInt(QUESTION_ID);
            mState = savedInstanceState.getInt(QUESTION_STATE);
            mName = savedInstanceState.getString(QUESTION_NAME);
            isFront = savedInstanceState.getInt(QUESTION_IS_FRONT);
        }

        // get the path
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            mainPath = Objects.requireNonNull(Objects.requireNonNull(getActivity()).getExternalFilesDir(
                    Environment.getRootDirectory().getPath()))
                    .getPath() + "/" + AppConfig.IMAGE_LOCATION_DIRECTORY;
        } else {
            mainPath = "/storage/emulated/0/Android/data/com.olafss.shamscorner.olafss_flashcard/files/system/"
                    + AppConfig.IMAGE_LOCATION_DIRECTORY;
        }

        // Inflate the question panel fragment layout
        final View rootView = inflater.inflate(R.layout.fragment_question_panel, container, false);

        // initialize the view model
        QuestionViewModel mQuestionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);

        // get the state text view and set the text
        TextView tvState = rootView.findViewById(R.id.tv_state);
        setState(tvState);

        // set the question in the root view
        // initiate the text view for the text
        final TextView tvText = rootView.findViewById(R.id.question_text_up);
        // do things in the background
        new SetQuestionImageAsyncTask(mQuestionViewModel, id, isFront) {
            @Override
            public void onResponseRecieve(ImageValue imageValue) {
                if (imageValue != null) {
                    // get the views for the image
                    ImageView imgJustImage = rootView.findViewById(R.id.question_image);
                    imgJustImage.setVisibility(View.VISIBLE);
                    // set the image to the image view
                    setImageViewInQuestionPanel(imgJustImage, imageValue.url);

                    if (mName.isEmpty()) {
                        tvText.setVisibility(View.GONE);
                    } else if (imageValue.position == 1) { // that means the image position up
                        tvText.setVisibility(View.GONE);

                        TextView tvTextBottom = rootView.findViewById(R.id.question_text_down);
                        tvTextBottom.setVisibility(View.VISIBLE);
                        tvTextBottom.setText(mName);

                    } else {
                        tvText.setText(mName);
                    }
                } else {
                    tvText.setText(mName);
                }
            }
        }.execute();

        // return the root view
        return rootView;
    }

    /**
     * set the state and data into the panel
     *
     * @param id      the question unique id
     * @param mState  the question state
     * @param mName   the data of the main panel
     * @param isFront if it is for front then true otherwise false
     */
    public void setData(int id, int mState, String mName, int isFront) {
        this.id = id;
        this.mState = mState;
        this.mName = mName;
        this.isFront = isFront;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle currentState) {
        currentState.putInt(QUESTION_ID, id);
        currentState.putInt(QUESTION_STATE, mState);
        currentState.putString(QUESTION_NAME, mName);
        currentState.putInt(QUESTION_IS_FRONT, isFront);
    }

    private void setImageViewInQuestionPanel(ImageView imageView, String url) {
        File imgFile = new  File(mainPath + url);

        if(imgFile.exists()){

            Bitmap imageBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            imageView.setImageBitmap(imageBitmap);

        }
    }

    /**
     * Set the question state for the user
     */
    private void setState(TextView tvState) {
        switch (mState) {
            case 0:
                tvState.setText(R.string.state_new);
                tvState.setBackgroundResource(R.drawable.tv_new);
                tvState.setTextColor(getResources().getColor(R.color.color_red));
                break;
            case 1:
                tvState.setText(R.string.state_learning);
                tvState.setBackgroundResource(R.drawable.tv_learning);
                tvState.setTextColor(getResources().getColor(R.color.color_orange_deep));
                break;
            case 2:
                tvState.setText(R.string.state_review);
                tvState.setBackgroundResource(R.drawable.tv_review);
                tvState.setTextColor(getResources().getColor(R.color.color_lime));
                break;
            case 3:
                tvState.setText(R.string.state_master);
                tvState.setBackgroundResource(R.drawable.tv_master);
                tvState.setTextColor(getResources().getColor(R.color.color_green_dark));
                break;
        }
    }

    /**
     * Set the question into the question panel
     */
    private interface QuestionImageValueInterface {
        void onResponseRecieve(ImageValue imageValue);
    }

    private static abstract class SetQuestionImageAsyncTask extends AsyncTask<Void, Void, Void>
            implements QuestionImageValueInterface {

        private QuestionViewModel questionViewModel;
        private ImageValue imageValue;
        private int id;
        private int isFront;

        SetQuestionImageAsyncTask(QuestionViewModel questionViewModel, int id, int isFront) {
            this.questionViewModel = questionViewModel;
            this.id = id;
            this.isFront = isFront;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // get the image for the question if available
            imageValue = questionViewModel.getImageData(id, isFront);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            onResponseRecieve(imageValue);
        }
    }
}
