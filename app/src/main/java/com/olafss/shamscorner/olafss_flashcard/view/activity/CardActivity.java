package com.olafss.shamscorner.olafss_flashcard.view.activity;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.entity.QuestionData;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.ProgressViewModel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.QuestionViewModel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.UserViewModel;
import com.olafss.shamscorner.olafss_flashcard.view.fragment.BackCardProgressFragment;
import com.olafss.shamscorner.olafss_flashcard.view.fragment.FrontCardButtonFragment;
import com.olafss.shamscorner.olafss_flashcard.view.fragment.QuestionPanelFragment;

public class CardActivity extends AppCompatActivity implements
        FrontCardButtonFragment.OnTouchToSeeClickListener,
        BackCardProgressFragment.OnTouchIKnowOrIDontKnowButton {

    /**
     * The key ID of the chapter number
     */
    private static final String CHAPTER_NUMBER = "CHAPTER_NUMBER";

    /**
     * The key ID of the subject part
     */
    private static final String SUBJECT_PART = "CHAPTER_PART";

    /**
     * The key ID of the question data
     */
    private static final String QUESTION_DATA = "question_data";

    /**
     * The chapter number from the previous activity that passed
     */
    private int chapterNumber;

    /**
     * The subject part from the previous activity that passed
     */
    private int subjectPart;

    /**
     * the level text view
     */
    private TextView tvLevel;

    /**
     * the fragment manager
     */
    private FragmentManager fragmentManager;

    /**
     * the question panel fragment
     */
    private QuestionPanelFragment questionPanelFragment;

    /**
     * the question data
     */
    private QuestionData questionData;

    /**
     * the question view model
     */
    private QuestionViewModel mQuestionViewModel;

    /**
     * the progress view model
     */
    private ProgressViewModel mProgressViewModel;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_panel);

        // get the chapter number given from the previous activity
        // get the bundle extras first from the current activity
        Bundle extras = getIntent().getExtras();
        // check whether the extras are empty or not, if not then get the number
        if (extras != null) {
            chapterNumber = extras.getInt(CHAPTER_NUMBER);
            subjectPart = extras.getInt(SUBJECT_PART);
        }

        // initialize the user view model
        UserViewModel mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        mQuestionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);
        mProgressViewModel = ViewModelProviders.of(this).get(ProgressViewModel.class);

        // get the level text view
        // the overall level of the current user from the user table
        tvLevel = findViewById(R.id.tv_level);
        mUserViewModel.getUserLevel().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer level) {
                setLevel(level);
            }
        });

        // now set the fragments
        // first get the fragment manager and transaction
        fragmentManager = getSupportFragmentManager();

        // create the question panel fragment
        questionPanelFragment = new QuestionPanelFragment();

        if (savedInstanceState == null) {

            // get the question data
            // the question data
            new GetQuestionAsyncTask(mQuestionViewModel, chapterNumber, subjectPart) {
                @Override
                public void onResponseReceive(QuestionData data) {

                    if(data == null){
                        // open up the finishing dialog activity
                        Intent finishedIntent = new Intent(getApplicationContext(), FinishedActivity.class);
                        finishedIntent.putExtra(CHAPTER_NUMBER, chapterNumber);
                        finishedIntent.putExtra(SUBJECT_PART, subjectPart);
                        startActivity(finishedIntent);
                        // make transition push left
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        // finish the activity
                        finish();
                    } else {
                        questionData = data;
                        //Log.i("QUESTION_DATA", questionData.toString());
                        // set the required data
                        questionPanelFragment.setData(questionData.getUniqueId(), questionData.getState(),
                                questionData.getName(), 0);
                        // add this fragment
                        fragmentManager.beginTransaction().add(R.id.fragment_question_panel, questionPanelFragment).commit();
                    }
                }
            }.execute();

            // create the front panel fragment for the touch to see section
            FrontCardButtonFragment frontCardButtonFragment = new FrontCardButtonFragment();
            // add this fragment
            fragmentManager.beginTransaction().add(R.id.fragment_body_panel, frontCardButtonFragment).commit();
        } else {
            questionData = savedInstanceState.getParcelable(QUESTION_DATA);
        }
    }

    /**
     * interface for the getting question data
     */
    private interface GetQuestionInterface {
        void onResponseReceive(QuestionData data);
    }

    /**
     * abstract async task for getting the question data
     */
    private static abstract class GetQuestionAsyncTask extends AsyncTask<Void, Void, Void>
            implements GetQuestionInterface {

        private QuestionViewModel mAsyncTask;
        private QuestionData data;
        private int chapter;
        private int part;

        GetQuestionAsyncTask(QuestionViewModel mAsyncTask, int chapter, int part) {
            this.mAsyncTask = mAsyncTask;
            this.chapter = chapter;
            this.part = part;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // get the count
            data = mAsyncTask.getQuestion(chapter, part);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            onResponseReceive(data);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(QUESTION_DATA, questionData);
    }

    @Override
    public void onTouchToSeeClicked() {
        // update things in the background for the touch to see button
        new UpdateThingsInBackgroundForTouchButton().execute();
    }

    @Override
    public void onSkipClicked() {
        // update things in the background for the skip button
        new UpdateThingsInBackgroundForSkipButton().execute();
    }

    @Override
    public void onBackPressed() {
        showDialog();
    }

    /**
     * show the dialog for exiting the card
     */
    public void showDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.dialog_exit_card_text);
        alertDialogBuilder.setTitle(getResources().getString(R.string.dialog_exit_card));
        alertDialogBuilder.setPositiveButton(R.string.exit,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // if it is ok
                        Intent setIntent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(setIntent);
                        finish();
                    }
                });

        alertDialogBuilder.setNegativeButton(R.string.stay,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // if it is not ok
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onTouchIKnow() {

        // update the i know counter
        mQuestionViewModel.updateIKnow(questionData.getUniqueId(), questionData.getIKnow() + 1);

        // update the progress value
        if (questionData.getState() < 3) {
            updateProgressAndQuestion(true, questionData.getState());
        }

        // set the next card
        setNextCard(false);

    }

    @Override
    public void onTouchIDontKnow() {

        // update the i don't know counter
        mQuestionViewModel.updateIDontKnow(questionData.getUniqueId(), questionData.getIDontKnow() + 1);

        // update the state to the new state as 0
        updateProgressAndQuestion(false, 0);

        // set the next card
        setNextCard(false);
    }

    @SuppressLint("StaticFieldLeak")
    private void setNextCard(final boolean isSkippedClicked) {

        // get the question data
        // the question data
        new GetQuestionAsyncTask(mQuestionViewModel, chapterNumber, subjectPart) {
            @Override
            public void onResponseReceive(QuestionData data) {
                //Log.i("QUESTION_DATA", questionData.toString());

                if (data == null) {
                    // open up the finishing dialog activity
                    Intent finishedIntent = new Intent(getApplicationContext(), FinishedActivity.class);
                    finishedIntent.putExtra(CHAPTER_NUMBER, chapterNumber);
                    finishedIntent.putExtra(SUBJECT_PART, subjectPart);
                    startActivity(finishedIntent);
                    // make transition push left
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                    // finish the activity
                    finish();
                } else {
                    // load the question data
                    questionData = data;

                    // reload the question panel fragment
                    // set the required data
                    questionPanelFragment.setData(questionData.getUniqueId(), questionData.getState(),
                            questionData.getName(), 0);
                    // add this fragment
                    fragmentManager.beginTransaction()
                            .setCustomAnimations(
                                    R.anim.card_appear_out,
                                    R.anim.card_appear_in)
                            .detach(questionPanelFragment)
                            .attach(questionPanelFragment)
                            .commit();

                    // check skip button clicked or not
                    if (!isSkippedClicked) {
                        // create the front panel fragment for the touch to see section
                        FrontCardButtonFragment frontCardButtonFragment = new FrontCardButtonFragment();
                        // add this fragment
                        fragmentManager.beginTransaction()
                                .replace(R.id.fragment_body_panel, frontCardButtonFragment)
                                .commit();
                    }
                }
            }
        }.execute();
    }

    /**
     * Set the level according to the user performance
     */
    private void setLevel(int level) {
        switch (level) {
            case 0:
                tvLevel.setText(R.string.level_beginner);
                tvLevel.setBackgroundResource(R.drawable.tv_beginner);
                tvLevel.setTextColor(getResources().getColor(R.color.color_orange));
                break;
            case 1:
                tvLevel.setText(R.string.level_advanced);
                tvLevel.setBackgroundResource(R.drawable.tv_advanced);
                tvLevel.setTextColor(getResources().getColor(R.color.color_orange_deep));
                break;
            case 2:
                tvLevel.setText(R.string.level_professional);
                tvLevel.setBackgroundResource(R.drawable.tv_professional);
                tvLevel.setTextColor(getResources().getColor(R.color.color_green_light));
                break;
        }
    }

    /*private interface MasterProgressValue {
        void onResponseReceive(int progressValue);
    }

    private static abstract class GetMasterProgressValueAsyncTask extends AsyncTask<Void, Void, Void>
            implements MasterProgressValue {

        private ProgressViewModel progressViewModel;
        private int chapter;
        private int part;

        private int progressValue;

        GetMasterProgressValueAsyncTask(ProgressViewModel progressViewModel, int chapter, int part) {
            this.progressViewModel = progressViewModel;
            this.chapter = chapter;
            this.part = part;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            progressValue = progressViewModel.getMasterProgressValue(chapter, part);

            Log.i("PROGRESS_VALUE_INNER", "progress - " + progressValue);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            onResponseReceive(progressValue);
        }
    }*/

    /**
     * update the progress values
     */
    @SuppressLint("StaticFieldLeak")
    private void updateProgressAndQuestion(boolean iKnow, int questionState) {
        // check if the question counter is within the state
        if (iKnow) {
            // increment the question state
            ++questionState;
        }

        // update the state for the question in the question table
        mQuestionViewModel.updateState(questionData.getUniqueId(), questionState);

        // update the progress in the progress table
        new UpdateForProgressAndQuestionAsyncTask(mQuestionViewModel, chapterNumber, subjectPart) {

            @Override
            public void onResponseReceive(final int count) {

                new GetForStatesAsyncTask(mQuestionViewModel, chapterNumber, subjectPart) {
                    @Override
                    public void getProgressState(int state0, int state1, int state2, int state3) {
                        mProgressViewModel.updateProgressData(
                                chapterNumber,
                                subjectPart,
                                (int) getPercentProgress(state0, count),
                                (int) getPercentProgress(state1, count),
                                (int) getPercentProgress(state2, count),
                                (int) getPercentProgress(state3, count)
                        );
                    }
                }
                        .execute();
            }
        }.execute();
    }

    /**
     * interface for updating the progress and question states
     */
    private interface UpdateForProgressAndQuestion {
        void onResponseReceive(int count);
    }

    /**
     * abstract class for updating the progress and question states
     */
    private static abstract class UpdateForProgressAndQuestionAsyncTask extends AsyncTask<Void, Void, Void>
            implements UpdateForProgressAndQuestion {

        private QuestionViewModel questionViewModel;
        private int count;
        private int chapter;
        private int part;

        UpdateForProgressAndQuestionAsyncTask(QuestionViewModel questionViewModel, int chapter, int part) {
            this.questionViewModel = questionViewModel;
            this.chapter = chapter;
            this.part = part;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // get the item
            count = questionViewModel.getTotalQuestionCount(part, chapter);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            onResponseReceive(count);
        }
    }

    /**
     * interface for getting the states of the specified question
     */
    private interface GetForStates {
        void getProgressState(int state0, int state1, int state2, int state3);
    }

    /**
     * abstract class for getting the states of the specified question
     */
    private static abstract class GetForStatesAsyncTask extends AsyncTask<Void, Void, Void>
            implements GetForStates {

        private QuestionViewModel questionViewModel;
        private int chapter;
        private int part;
        private int state0;
        private int state1;
        private int state2;
        private int state3;

        GetForStatesAsyncTask(QuestionViewModel questionViewModel, int chapter, int part) {
            this.questionViewModel = questionViewModel;
            this.chapter = chapter;
            this.part = part;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // get the item
            state0 = questionViewModel.getProgressState(part, chapter, 0);
            state1 = questionViewModel.getProgressState(part, chapter, 1);
            state2 = questionViewModel.getProgressState(part, chapter, 2);
            state3 = questionViewModel.getProgressState(part, chapter, 3);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            getProgressState(state0, state1, state2, state3);
        }
    }

    /**
     * the background process for the skip button
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateThingsInBackgroundForSkipButton extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            // update the skipped counter
            mQuestionViewModel.updateSkipped(questionData.getUniqueId(), questionData.getSkipped() + 1);

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            // set the next card
            setNextCard(true);
        }
    }

    /**
     * the background process for the touch to see button
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateThingsInBackgroundForTouchButton extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            // update the flipped counter
            mQuestionViewModel.updateFlipped(questionData.getUniqueId(), questionData.getFlipped() + 1);

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            // reload the question panel fragment
            // set the required data
            questionPanelFragment.setData(questionData.getUniqueId(), questionData.getState(),
                    questionData.getAnswer(), 1);
            // add this fragment
            fragmentManager.beginTransaction()
                    .setCustomAnimations(
                            R.animator.card_flip_right_in,
                            R.animator.card_flip_right_out,
                            R.animator.card_flip_left_in,
                            R.animator.card_flip_left_out)
                    .detach(questionPanelFragment)
                    .attach(questionPanelFragment)
                    .commit();

            // create the back panel fragment for the touch to see section
            BackCardProgressFragment backCardProgressFragment = new BackCardProgressFragment();
            // set the required data
            backCardProgressFragment.setData(questionData.getChapter(), questionData.getPart());

            // replace the front fragment to the back fragment
            fragmentManager.beginTransaction().replace(R.id.fragment_body_panel, backCardProgressFragment).commit();
        }
    }

    /**
     * Get the percent form of the progress value
     *
     * @param val            the progress value
     * @param questionNumber the total number of the questions
     * @return the percent form of the value
     */
    private float getPercentProgress(float val, int questionNumber) {
        return (val / questionNumber) * 100;
    }
}
