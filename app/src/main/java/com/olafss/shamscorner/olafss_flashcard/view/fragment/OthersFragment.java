package com.olafss.shamscorner.olafss_flashcard.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.data.OthersData;
import com.olafss.shamscorner.olafss_flashcard.view.controller.ListItemRecycleNewsFeed;
import com.olafss.shamscorner.olafss_flashcard.view.controller.ListItemRecyclerOthers;

import java.util.ArrayList;
import java.util.List;

public class OthersFragment extends Fragment {

    public OthersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewHolder = inflater.inflate(R.layout.fragment_others, container, false);

        // initiate the recycler view
        RecyclerView recyclerView = viewHolder.findViewById(R.id.recyclerview);

        // set the recycler view contents and adapter
        ListItemRecyclerOthers adapter = new ListItemRecyclerOthers();
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // todo: change this into dynamic list with glide and online sync
        List<OthersData> othersDataList = new ArrayList<>();
        othersDataList.add(new OthersData("Physics", R.drawable.icon_level_up_group));
        othersDataList.add(new OthersData("Chemistry", R.drawable.icon_join_group));
        othersDataList.add(new OthersData("Physics", R.drawable.icon_info_group));
        othersDataList.add(new OthersData("Math", R.drawable.icon_level_up_group));
        othersDataList.add(new OthersData("Physics", R.drawable.icon_level_up_group));
        othersDataList.add(new OthersData("English", R.drawable.icon_join_group));
        othersDataList.add(new OthersData("Physics", R.drawable.icon_level_up_group));
        othersDataList.add(new OthersData("Biology", R.drawable.icon_buy));

        // set the item list into the adapter
        adapter.setItems(othersDataList);

        // set the adapter into the recycler view
        recyclerView.setAdapter(adapter);

        // return the parent view
        return viewHolder;
    }
}
