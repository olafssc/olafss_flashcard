package com.olafss.shamscorner.olafss_flashcard.database.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Delete;
import android.os.AsyncTask;

import com.olafss.shamscorner.olafss_flashcard.database.FlashCardRoomDatabase;
import com.olafss.shamscorner.olafss_flashcard.database.dao.ProgressDataDao;
import com.olafss.shamscorner.olafss_flashcard.database.data.ProgressValue;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ProgressData;

import java.util.List;

public class ProgressDataRepository {

    /**
     * add the dao object
     */
    private ProgressDataDao mProgressDataDao;

    /**
     * construct the progress data repository
     *
     * @param application the main application object
     */
    public ProgressDataRepository(Application application) {
        // get the database
        FlashCardRoomDatabase db = FlashCardRoomDatabase.getDatabase(application);

        // get and set the dao
        mProgressDataDao = db.progressDataDao();
    }

    /**
     * get the all data of the progress
     *
     * @return the progress data as a list
     */
    public LiveData<List<ProgressData>> getProgressData() {
        return mProgressDataDao.getProgressData();
    }

    /**
     * delete all the data of the progress
     */
    public void deleteProgressData() {
        mProgressDataDao.deleteProgressData();
    }

    /**
     * get the total number of progress
     *
     * @return the progress count
     */
    public int getProgressDataCount() {
        return mProgressDataDao.getProgressDataCount();
    }

    /**
     * insert progress data as list
     *
     * @param progressDataList a list of progress data
     */
    public void insertProgressList(List<ProgressData> progressDataList) {
        new insertAsyncTask(mProgressDataDao, progressDataList).execute();
    }

    /**
     * insert the progress data in the progress table in background
     */
    private static class insertAsyncTask extends AsyncTask<Void, Void, Void> {

        /**
         * the progress data dao
         */
        private ProgressDataDao mAsyncTaskDao;

        /**
         * list of progress data
         */
        private List<ProgressData> progressDataList;

        insertAsyncTask(ProgressDataDao mAsyncTaskDao, List<ProgressData> progressDataList) {
            this.mAsyncTaskDao = mAsyncTaskDao;
            this.progressDataList = progressDataList;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // insert the data list
            mAsyncTaskDao.insertProgressList(progressDataList);
            return null;
        }
    }

    /**
     * get the chapter number of part 1
     */
    public int getChapterCountForPartOne() {
        return mProgressDataDao.getChapterCountForPartOne();
    }

    /**
     * reset the progress data for the specified chapter and part
     *
     * @param chapter the chapter number
     * @param part    the subject part
     */
    public void resetProgressData(int chapter, int part) {
        new ResetProgressDataAsyncTask(mProgressDataDao, chapter, part).execute();
    }

    private static class ResetProgressDataAsyncTask extends AsyncTask<Void, Void, Void> {

        private ProgressDataDao mAsyncTask;
        private int chapter;
        private int part;

        ResetProgressDataAsyncTask(ProgressDataDao mAsyncTask, int chapter, int part) {
            this.mAsyncTask = mAsyncTask;
            this.chapter = chapter;
            this.part = part;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // reset the data
            mAsyncTask.resetProgressData(chapter, part);

            return null;
        }
    }

    /**
     * update the progress data into the given chapter and part
     *
     * @param chapter     the chapter
     * @param part        the subject part
     * @param newVal      the new value
     * @param learningVal the learning value
     * @param reviewVal   the review value
     * @param masterVal   the master value
     */
    public void updateProgressData(int chapter, int part, int newVal, int learningVal,
                                   int reviewVal, int masterVal) {
        new updateProgressDataAsyncTask(mProgressDataDao, chapter, part, newVal, learningVal,
                reviewVal, masterVal).execute();
    }

    private static class updateProgressDataAsyncTask extends AsyncTask<Void, Void, Void> {

        private ProgressDataDao mAsyncTask;
        private int chapter;
        private int part;
        private int newVal;
        private int learningVal;
        private int reviewVal;
        private int masterVal;

        updateProgressDataAsyncTask(ProgressDataDao mAsyncTask, int chapter, int part, int newVal, int learningVal,
                                    int reviewVal, int masterVal) {
            this.mAsyncTask = mAsyncTask;
            this.chapter = chapter;
            this.part = part;
            this.newVal = newVal;
            this.learningVal = learningVal;
            this.reviewVal = reviewVal;
            this.masterVal = masterVal;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // update the data
            mAsyncTask.updateProgressData(chapter, part, newVal, learningVal, reviewVal, masterVal);

            return null;
        }
    }

    /**
     * get the master progress value from the given chapter number and part
     *
     * @param chapter the chapter number
     * @param part    the subject part
     * @return the master value
     */
    public int getMasterProgressValue(int chapter, int part) {
        return mProgressDataDao.getMasterProgressValue(chapter, part);
    }

    /**
     * get a progress row from the given chapter and part
     *
     * @param chapter the chapter number
     * @param part    the subject part
     * @return the progress value object (new, learning, review, master)
     */
    public ProgressValue getAProgressData(int chapter, int part) {
        return mProgressDataDao.getAProgressData(chapter, part);
    }
}
