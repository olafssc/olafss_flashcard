package com.olafss.shamscorner.olafss_flashcard.config;

public class AppConfig {

    /** server user login url */
    public static String URL_LOGIN = "https://olafssc.000webhostapp.com/login_api/login.php";

    /** server user register url */
    public static String URL_REGISTER = "https://olafssc.000webhostapp.com/login_api/register.php";

    /** server user, validating the username url */
    public static String URL_USERNAME_VALIDATION = "https://olafssc.000webhostapp.com/login_api/validate_username.php";

    /** server question, sync the all questions */
    public static String URL_SYNC_QUESTIONS = "https://olafssc.000webhostapp.com/login_api/get_questions.php";

    /** server question, sync all the states of the questions */
    public static String URL_SYNC_STATES = "https://olafssc.000webhostapp.com/login_api/get_states.php";

    /** the subject code */
    public static final String APP_SUBJECT_CODE = "Physics-HSC"; // it has 2 parts(part-1, part-2)

    /** the subject name */
    public static final String APP_SUBJECT_NAME = "HSC - Physics";

    /** the url for downloading the zip file of the question images */
    public static final String URL_ZIP = "https://olafssc.000webhostapp.com/5f1e651rg6516g55ew1f6e51f6e1g5eg1/";

    /** the name of the file to download */
    public static final String FILE_NAME_ZIP = "olafss_physics_hsc.zip";

    /** the name of the image directory */
    public static final String IMAGE_LOCATION_DIRECTORY = "olafss_physics_hsc/";

    /** the client id for signing information for the google */
    public static final String CLIENT_ID_GOOGLE = "514974182917-4vkuhqlctlkg70836v57mahkbug4dud5.apps.googleusercontent.com";

    public static final String URL_REGISTER_ONE_CLICK = "https://olafssc.000webhostapp.com/login_api/register_one_click.php";

    public static final String URL_LOGIN_ONE_CLICK = "https://olafssc.000webhostapp.com/login_api/login_one_click.php";
}

