package com.olafss.shamscorner.olafss_flashcard.database.data;

import android.arch.persistence.room.ColumnInfo;

public class ChapterNameAndNumber {

    /** the chapter name */
    @ColumnInfo(name = "name")
    public String name;

    /** the chapter number */
    @ColumnInfo(name = "number")
    public int number;
}
