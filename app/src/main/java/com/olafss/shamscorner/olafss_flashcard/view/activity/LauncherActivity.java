package com.olafss.shamscorner.olafss_flashcard.view.activity;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.config.AppConfig;
import com.olafss.shamscorner.olafss_flashcard.config.AppController;
import com.olafss.shamscorner.olafss_flashcard.database.data.ChapterNameAndNumber;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ProgressData;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.ProgressViewModel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.QuestionViewModel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.UserViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LauncherActivity extends AppCompatActivity {

    /**
     * the main tag for logging
     */
    private static final String TAG = LauncherActivity.class.getSimpleName();

    /**
     * the progress data view model
     */
    private ProgressViewModel mProgressViewModel;

    /**
     * the question data view model
     */
    private QuestionViewModel mQuestionViewModel;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        // get those view model first
        final UserViewModel mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        mQuestionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);
        mProgressViewModel = ViewModelProviders.of(this).get(ProgressViewModel.class);

        // TODO: create a any error table (any_error) if not exist

        new syncUserId(mUserViewModel) {
            @Override
            public void onResponseRecieve(String userId) {
                // sync data from the server into the question table in the states section
                syncStates(userId, AppConfig.APP_SUBJECT_CODE);
            }
        }.execute();
    }

    private interface GetUserID {
        void onResponseRecieve(String userId);
    }

    /**
     * insert the user data in the user table in background
     */
    private static abstract class syncUserId extends AsyncTask<Void, Void, Void>
            implements GetUserID {

        private UserViewModel userViewModel;
        private String userId;

        syncUserId(UserViewModel userViewModel) {
            this.userViewModel = userViewModel;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // get the user id
            userId = userViewModel.getUserId();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            onResponseRecieve(userId);
        }
    }

    /**
     * set all the corresponding value in the progress bar
     *
     * @param part the subject part
     */
    private void setProgressDatabase(int part) {

        // get the chapter list as array list for part
        List<ChapterNameAndNumber> chapterList = mQuestionViewModel.getChapterNameAndNumberList(part);

        // list of progress data
        List<ProgressData> progressDataList = new ArrayList<>();

        // traverse through items
        for (ChapterNameAndNumber chapter : chapterList) {

            // add the data into the progress data list
            progressDataList.add(new ProgressData(part, chapter.number, chapter.name,
                    (int) getPercentProgress(mQuestionViewModel.getProgressState(part, chapter.number, 0),
                            chapter.number, part),
                    (int) getPercentProgress(mQuestionViewModel.getProgressState(part, chapter.number, 1),
                            chapter.number, part),
                    (int) getPercentProgress(mQuestionViewModel.getProgressState(part, chapter.number, 2),
                            chapter.number, part),
                    (int) getPercentProgress(mQuestionViewModel.getProgressState(part, chapter.number, 3),
                            chapter.number, part)));

        }

        // insert into the progress table
        mProgressViewModel.insertProgressList(progressDataList);
    }

    /**
     * Get the percent form of the progress value
     *
     * @param val     the progress value
     * @param chapter the subject chapter
     * @param part    the subject part
     * @return the percent form of the value
     */
    private float getPercentProgress(float val, int chapter, int part) {
        return (val / mQuestionViewModel.getTotalQuestionCount(part, chapter)) * 100;
    }

    /**
     * the background process
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateThingsInBackground extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {


            // update the progress table from the question table
            // delete all the current progress table rows first
            mProgressViewModel.deleteProgressData();

            // set the new progress into the progress table
            setProgressDatabase(1); // for part 1
            setProgressDatabase(2); // for part 2

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            // open up the Main activity and clear the back stack
            Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(mainActivityIntent);
            // override the animation
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            // finish the activity when work is done
            finish();
        }
    }

    /**
     * method to sync all the states of the questions from the server
     *
     * @param username    the user username
     * @param subjectCode the subject code of this current flashcard
     */
    private void syncStates(final String username, final String subjectCode) {

        // Tag used to cancel the request
        String tag_string_req = "req_sync_states";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_SYNC_STATES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Question Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // questions synced successfully
                        // get the state from the server and put a state array in the json
                        JSONArray stateList = jObj.getJSONArray("states");
                        if (stateList.length() > 0) {
                            mQuestionViewModel.updateStateList(stateList);
                        }

                    }

                    // start the background task activities
                    new UpdateThingsInBackground().execute();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.something_wrong),
                            Toast.LENGTH_LONG).show();
                    // start the background task activities
                    new UpdateThingsInBackground().execute();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Question Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_connection),
                        Toast.LENGTH_LONG).show();
                // start the background task activities
                new UpdateThingsInBackground().execute();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("user_id", username);
                params.put("sub_code", subjectCode);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
