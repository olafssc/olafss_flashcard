package com.olafss.shamscorner.olafss_flashcard.view.controller;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.olafss.shamscorner.olafss_flashcard.R;

import java.util.List;

public class SliderAdapterForIntro extends PagerAdapter {

    /**
     * the application context
     */
    private Context context;

    /**
     * the corresponding image for the intro
     */
    private List<Integer> imageId;

    public SliderAdapterForIntro(
            Context context,
            List<Integer> imageId) {
        this.context = context;
        this.imageId = imageId;
    }

    @Override
    public int getCount() {
        return imageId.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        // get the layout inflater to inflate the item layout
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // get the parent view and inflate the item layout
        View view = inflater.inflate(R.layout.item_slider_intro, null);

        // get the items from the parent view
        // get the image view
        ImageView imageView = view.findViewById(R.id.imageview);

        // set the contents in the items
        // set the image to the image view
        imageView.setImageResource(imageId.get(position));

        // get the view pager and add the view
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        // return the parent view
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
