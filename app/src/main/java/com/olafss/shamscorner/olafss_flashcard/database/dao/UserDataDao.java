package com.olafss.shamscorner.olafss_flashcard.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.olafss.shamscorner.olafss_flashcard.database.data.NameAndLevel;
import com.olafss.shamscorner.olafss_flashcard.database.entity.UserData;

@Dao
public interface UserDataDao {

    /** insert the userdata into the user table */
    @Insert
    void insertUser(UserData userData);

    /** get the all information of the user */
    @Query("SELECT * FROM user")
    UserData getUserData();

    /** delete the current user */
    @Query("DELETE FROM user")
    void deleteUserData();

    /** get the name and the level of the user */
    @Query("SELECT name, level FROM user")
    LiveData<NameAndLevel> getNameAndLevel();

    /** get the user id */
    @Query("SELECT user_id FROM user")
    String getUserId();

    /** get the user level */
    @Query("SELECT level FROM user")
    LiveData<Integer> getUserLevel();
}
