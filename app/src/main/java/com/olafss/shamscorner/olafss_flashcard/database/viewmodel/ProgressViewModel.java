package com.olafss.shamscorner.olafss_flashcard.database.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.olafss.shamscorner.olafss_flashcard.database.data.ProgressValue;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ProgressData;
import com.olafss.shamscorner.olafss_flashcard.database.repository.ProgressDataRepository;

import java.util.List;

public class ProgressViewModel extends AndroidViewModel {

    /**
     * the repository for the progress data
     */
    private ProgressDataRepository mProgressDataRepository;

    /**
     * construct the view model for the progress data
     *
     * @param application the main application object
     */
    public ProgressViewModel(Application application) {
        super(application);

        // get the progress data repository
        mProgressDataRepository = new ProgressDataRepository(application);
    }

    /**
     * get all the progress from the progress table
     *
     * @return a list of all progress data
     */
    public LiveData<List<ProgressData>> getProgressData() {
        return mProgressDataRepository.getProgressData();
    }

    /**
     * delete all the data of the progress
     */
    public void deleteProgressData() {
        mProgressDataRepository.deleteProgressData();
    }

    /**
     * get the total number of progress
     *
     * @return the progress count
     */
    public int getProgressDataCount() {
        return mProgressDataRepository.getProgressDataCount();
    }

    /**
     * insert progress data as list
     *
     * @param progressDataList a list of progress data
     */
    public void insertProgressList(List<ProgressData> progressDataList) {
        mProgressDataRepository.insertProgressList(progressDataList);
    }

    /**
     * get the chapter number of part 1
     */
    public int getChapterCountForPartOne() {
        return mProgressDataRepository.getChapterCountForPartOne();
    }

    /**
     * reset the progress data for the specified chapter and part
     *
     * @param chapter the chapter number
     * @param part    the subject part
     */
    public void resetProgressData(int chapter, int part) {
        mProgressDataRepository.resetProgressData(chapter, part);
    }

    /**
     * update the progress data into the given chapter and part
     *
     * @param chapter     the chapter
     * @param part        the subject part
     * @param newVal      the new value
     * @param learningVal the learning value
     * @param reviewVal   the review value
     * @param masterVal   the master value
     */
    public void updateProgressData(int chapter, int part, int newVal, int learningVal,
                                   int reviewVal, int masterVal) {
        mProgressDataRepository.updateProgressData(chapter, part, newVal, learningVal, reviewVal, masterVal);
    }

    /**
     * get the master progress value from the given chapter number and part
     *
     * @param chapter the chapter number
     * @param part    the subject part
     * @return the master value
     */
    public int getMasterProgressValue(int chapter, int part) {
        return mProgressDataRepository.getMasterProgressValue(chapter, part);
    }

    /**
     * get a progress row from the given chapter and part
     *
     * @param chapter the chapter number
     * @param part    the subject part
     * @return the progress value object (new, learning, review, master)
     */
    public ProgressValue getAProgressData(int chapter, int part) {
        return mProgressDataRepository.getAProgressData(chapter, part);
    }
}
