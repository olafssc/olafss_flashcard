package com.olafss.shamscorner.olafss_flashcard.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.olafss.shamscorner.olafss_flashcard.database.data.ChapterNameAndNumber;
import com.olafss.shamscorner.olafss_flashcard.database.data.ChapterNumberAndPart;
import com.olafss.shamscorner.olafss_flashcard.database.data.CountData;
import com.olafss.shamscorner.olafss_flashcard.database.data.ImageValue;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ChapterData;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ImageData;
import com.olafss.shamscorner.olafss_flashcard.database.entity.QuestionData;

import java.util.List;

@Dao
public interface QuestionDataDao {

    /**
     * get the chapter name and number as a list
     *
     * @param part the subject part
     * @return the list of name and number of the chapter
     */
    @Query("SELECT name, number FROM chapters WHERE part = :part")
    List<ChapterNameAndNumber> getChapterNameAndNumberList(int part);

    /** insert the chapter data into the chapter table */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertChapterList(List<ChapterData> chapterDataList);

    /** delete the current chapter data */
    @Query("DELETE FROM chapters")
    void deleteChapterData();

    /** insert the question data into the question table */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertQuestionList(List<QuestionData> questionDataList);

    /**
     * get the number of questions with the specified chapter, part and state
     *
     * @param part    the subject part
     * @param chapter the chapter number
     * @param state   the current state (0-new, 1-learning, 2-review, 3-master)
     * @return the number of count of the questions
     */
    @Query("SELECT COUNT(unique_id) FROM questions WHERE chapter = :chapter AND part = :part AND state = :state")
    int getProgressState(int part, int chapter, int state);

    /**
     * get the total number of questions in the given chapter and part
     *
     * @param part    the subject part
     * @param chapter the chapter number
     * @return the total number of questions
     */
    @Query("SELECT COUNT(unique_id) FROM questions WHERE chapter = :chapter AND part = :part")
    int getTotalQuestionCount(int part, int chapter);

    /**
     * update the questions state in the given question
     * @param id the question id
     * @param state the current state (0-new, 1-learning, 2-review, 3-master)
     * @param iKnow i know counter
     * @param iDontKnow i don't know counter
     * @param flipped flipped counter
     * @param skipped skipped counter
     */
    @Query("UPDATE questions SET state = :state, i_know = :iKnow, i_dont_know = :iDontKnow, flipped = :flipped," +
            " skipped = :skipped WHERE unique_id = :id")
    void updateStateList(int id, int state, int iKnow, int iDontKnow, int flipped, int skipped);

    /** update the paid and id for the each user */
    @Query("UPDATE questions SET paid = :paid WHERE unique_id = :id")
    void updateIdAndPaidQuestions(int id, int paid);

    /** delete the current question data */
    @Query("DELETE FROM questions")
    void deleteQuestionData();

    /** insert the image data into the image table */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertImageList(List<ImageData> imageDataList);

    /** get the image data from the image table */
    @Query("SELECT url, position FROM images WHERE unique_id = :id AND type = :isFront")
    ImageValue getImageData(int id, int isFront);

    /** delete the current image data */
    @Query("DELETE FROM images")
    void deleteImageData();

    /** reset the question states for the specified chapter and part */
    @Query("UPDATE questions SET state = 0, i_know = 0, i_dont_know = 0 WHERE chapter = :chapter AND part = :part")
    void resetQuestionState(int chapter, int part);

    /** get the question data  */
    @Query("SELECT * FROM questions WHERE chapter = :chapter AND part = :part AND state != 3 ORDER BY RANDOM() LIMIT 1")
    QuestionData getQuestion(int chapter, int part);

    /** update the state in the question table */
    @Query("UPDATE questions SET state = :val WHERE unique_id = :id")
    void updateState(int id, int val);

    /** update the flipped counter in the question table */
    @Query("UPDATE questions SET flipped = :val WHERE unique_id = :id")
    void updateFlipped(int id, int val);

    /** update the skipped counter in the question table */
    @Query("UPDATE questions SET skipped = :val WHERE unique_id = :id")
    void updateSkipped(int id, int val);

    /** update the i know counter in the question table */
    @Query("UPDATE questions SET i_know = :val WHERE unique_id = :id")
    void updateIKnow(int id, int val);

    /** update the i don't know counter in the question table */
    @Query("UPDATE questions SET i_dont_know = :val WHERE unique_id = :id")
    void updateIDontKnow(int id, int val);

    /** get the all state counter object as a live data */
    @Query("SELECT SUM(i_know) AS i_know, SUM(i_dont_know) AS i_dont_know, SUM(skipped) " +
            "AS skipped, SUM(flipped) AS flipped FROM questions")
    LiveData<CountData> getCountData();

    /** get the recommendation for the check more panel grid view */
    @Query("SELECT chapter, part FROM questions WHERE i_dont_know != 0 AND " +
            "flipped != 0 GROUP BY chapter ORDER BY i_dont_know DESC LIMIT 12")
    LiveData<List<ChapterNumberAndPart>> getRecommendations();
}
