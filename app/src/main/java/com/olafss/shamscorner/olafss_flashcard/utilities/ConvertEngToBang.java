package com.olafss.shamscorner.olafss_flashcard.utilities;

public class ConvertEngToBang {

    /**
     * list of bangla character set
     */
    private static final String[] NUMLISTINBANGLA = {
            "০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯"
    };

    /**
     * get the number in bangla format
     *
     * @param num the number in english format
     * @return number converted to bangla text
     */
    public static String getNumberInBanglaFormat(int num) {

        StringBuilder mNumInBangla = new StringBuilder("");

        while (num > 0) {
            mNumInBangla.append(NUMLISTINBANGLA[num % 10]);
            num = num / 10;
        }

        return mNumInBangla.toString();
    }
}
