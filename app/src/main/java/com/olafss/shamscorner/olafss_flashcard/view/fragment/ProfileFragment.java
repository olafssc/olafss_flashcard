package com.olafss.shamscorner.olafss_flashcard.view.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.data.ChapterNumberAndPart;
import com.olafss.shamscorner.olafss_flashcard.database.data.CountData;
import com.olafss.shamscorner.olafss_flashcard.database.data.NameAndLevel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.QuestionViewModel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.UserViewModel;
import com.olafss.shamscorner.olafss_flashcard.utilities.ConvertEngToBang;
import com.olafss.shamscorner.olafss_flashcard.view.activity.CardActivity;
import com.olafss.shamscorner.olafss_flashcard.view.activity.GlideApp;

import java.util.List;

public class ProfileFragment extends Fragment {

    /** the main reference activity */
    private Activity referenceActivity;

    /** text view for the flipped counter */
    private TextView tvFlipped;

    /** text view for the skipped counter */
    private TextView tvSkipped;

    /** text view for the i know counter */
    private TextView tvIKnow;

    /** text view for the i don't know counter */
    private TextView tvIDontKnow;

    /** text view for the knowing ratio */
    private TextView tvKnowingRatio;

    /** text view for the knowing ratio text */
    private TextView tvKnowingRatioText;

    /** text view for the level text */
    private TextView tvLevel;

    /** The key ID of the chapter number */
    private static final String CHAPTER_NUMBER =  "CHAPTER_NUMBER";

    /** The key ID of the subject part */
    private static final String SUBJECT_PART =  "CHAPTER_PART";

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // the main parent holder layout
        View parentHolder = inflater.inflate(R.layout.fragment_profile, container, false);

        // get the main activity to the reference activity
        referenceActivity = getActivity();

        // initiate the user database object
        // user view model for the user table
        UserViewModel mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        /* the question view model for the question table */
        QuestionViewModel mQuestionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);

        // set the profile image
        ImageView profileImageView = parentHolder.findViewById(R.id.profile_image);
        GlideApp.with(this)
                .load("https://australiavisaservices.com/images/blog/passportphoto-front.jpg")
                .centerCrop()
                .placeholder(R.drawable.icon_user)
                .into(profileImageView);

        // set the name and level
        // text view for the name text
        final TextView tvName = parentHolder.findViewById(R.id.profile_name);
        tvLevel = parentHolder.findViewById(R.id.profile_level);

        mUserViewModel.getNameAndLevel().observe(this, new Observer<NameAndLevel>() {
            @Override
            public void onChanged(NameAndLevel nameAndLevel) {

                if(nameAndLevel != null) {
                    tvName.setText(nameAndLevel.name);
                    setLevel(nameAndLevel.level);
                }
            }
        });

        // set the text for the state textview for the user
        tvFlipped = parentHolder.findViewById(R.id.tv_flipped);
        tvSkipped = parentHolder.findViewById(R.id.tv_skipped);
        tvIDontKnow = parentHolder.findViewById(R.id.tv_dont_know);
        tvIKnow = parentHolder.findViewById(R.id.tv_know);

        // set the knowing ratio text views
        tvKnowingRatio = parentHolder.findViewById(R.id.knowing_ratio);
        tvKnowingRatioText = parentHolder.findViewById(R.id.knowing_ratio_text);

        // set the text to the views as background task, and also the knowing ratio
        mQuestionViewModel.getCountData().observe(this, new Observer<CountData>() {
            @Override
            public void onChanged(CountData countData) {
                setOnStates(countData.iKnowCount, countData.iDontKnowCount, countData.flippedCount, countData.skippedCount);
            }
        });

        // get the parent holder first
        final GridLayout lookMorePanel = parentHolder.findViewById(R.id.look_more_holder);

        // add chapters in the chapter recommendations panel
        //addChaptersInLookMorePanel();
        mQuestionViewModel.getRecommendations().observe(this, new Observer<List<ChapterNumberAndPart>>() {
            @Override
            public void onChanged(List<ChapterNumberAndPart> chapterNumberAndPartsList) {
                for(ChapterNumberAndPart chapterNumberAndPart:chapterNumberAndPartsList) {
                    lookMorePanel.addView(getTextview(chapterNumberAndPart.chapter, chapterNumberAndPart.part));
                }
            }
        });

        // set the action listener for the edit profile button
        ImageButton btnEditProfile = parentHolder.findViewById(R.id.edit_profile);
        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // handle the edit profile section
                editProfileSection();
            }
        });

        // return the parent holder
        return parentHolder;
    }

    /**
     * handle the edit profile section
     */
    private void editProfileSection() {
        // open a full screen dialog
        ProfileEditDialogFragment dialog = new ProfileEditDialogFragment();
        if (getFragmentManager() != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            dialog.show(ft, ProfileEditDialogFragment.TAG);
        }
    }

    /**
     * Set the level from the user table
     */
    private void setLevel(int userLevel) {
        switch (userLevel){
            case 0:
                tvLevel.setText(R.string.level_beginner);
                break;
            case 1:
                tvLevel.setText(R.string.level_advanced);
                break;
            case 2:
                tvLevel.setText(R.string.level_professional);
                break;
        }
    }

    /**
     * set the textview value with the total count value
     */
    public void setOnStates(long iKnowCount, long iDontKnowCount, long flippedCount, long skippedCount){
        String flipped, skipped, iKnow, iDontKnow;

        //Log.i("COUNT_KNOW", "know count " + iKnowCount);
        //Log.i("COUNT_DONT_KNOW", "know count " + iDontKnowCount);

        // get the percent value
        int percentVal;
        if(iKnowCount == 0){
            percentVal = 0;
        }else if(iDontKnowCount == 0){
            percentVal = 100;
        }else{
            percentVal = (int)(((double)(iKnowCount - iDontKnowCount) / iKnowCount) * 100);
        }

        // set the percent to the percent text view
        String percent = percentVal + "%";
        tvKnowingRatio.setText(percent);

        // get the recommend text
        if(percentVal > 74){
            tvKnowingRatio.setTextColor(getResources().getColor(R.color.color_green_light));
            tvKnowingRatioText.setText(getResources().getString(R.string.message_master));
        }else if(percentVal > 49){
            tvKnowingRatio.setTextColor(getResources().getColor(R.color.color_orange_deep));
            tvKnowingRatioText.setText(getResources().getString(R.string.message_review));
        }else if(percentVal > 24){
            tvKnowingRatio.setTextColor(getResources().getColor(R.color.color_yellow));
            tvKnowingRatioText.setText(getResources().getString(R.string.message_learning));
        }else{
            tvKnowingRatio.setTextColor(getResources().getColor(R.color.color_orange));
            tvKnowingRatioText.setText(getResources().getString(R.string.message_new));
        }

        flipped = convertToSuffix(flippedCount);
        skipped = convertToSuffix(skippedCount);
        iKnow = convertToSuffix(iKnowCount);
        iDontKnow = convertToSuffix(iDontKnowCount);

        tvFlipped.setText(flipped);
        tvSkipped.setText(skipped);
        tvIKnow.setText(iKnow);
        tvIDontKnow.setText(iDontKnow);
    }

    /**
     * convert the counter to readable to human eye
     * @param count the counter
     * @return readable counter
     */
    @SuppressLint("DefaultLocale")
    // https://stackoverflow.com/questions/9769554/how-to-convert-number-into-k-thousands-m-million-and-b-billion-suffix-in-jsp
    // Converts the number to K, M suffix
    // Ex: 5500 will be displayed as 5.5k
    public static String convertToSuffix(long count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format("%.1f%c",
                count / Math.pow(1000, exp),
                "kmgtpe".charAt(exp - 1));
    }

    /**
     * get the textview for the cloud type textview panel
     * @param chapter the chapter number
     * @param part the subject part
     * @return a textview with total text and color and also layout randomly colored
     */
    private TextView getTextview(final int chapter, final int part){

        // make a text view
        TextView tv = new TextView(referenceActivity);
        String s =  getResources().getString(R.string.chapter) + " "
                + ConvertEngToBang.getNumberInBanglaFormat(part) + "."
                + ConvertEngToBang.getNumberInBanglaFormat(chapter);

        // set the text to the textview
        tv.setText(s);
        tv.setTextSize(12);

        // set the text color as white
        tv.setTextColor(Color.BLACK);

        // set the padding
        tv.setPadding((int)getResources().getDimension(R.dimen.margin_default),
                (int)getResources().getDimension(R.dimen.margin_default),
                (int)getResources().getDimension(R.dimen.margin_default),
                (int)getResources().getDimension(R.dimen.margin_default));

        // set the margin
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins((int)getResources().getDimension(R.dimen.padding_default_2),
                (int)getResources().getDimension(R.dimen.padding_default_2),
                (int)getResources().getDimension(R.dimen.padding_default_2),
                (int)getResources().getDimension(R.dimen.padding_default_2));
        tv.setLayoutParams(params);

        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(40);
        shape.setColor(getResources().getColor(R.color.colorAccent));

        // Finally, add the drawable background to TextView
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            tv.setBackground(shape);
        }else{
            tv.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.i("CLICKED", "chapter - " + chapter + ", part - " + part);
                openCard(chapter, part);
            }
        });

        // return the textview
        return tv;
    }

    /**
     * open up the card in the specified chapter and part
     * @param subjectChapter chapter number
     * @param subjectPart part number
     */
    private void openCard(int subjectChapter, int subjectPart){
        Intent frontCardIntent = new Intent(referenceActivity, CardActivity.class);
        frontCardIntent.putExtra(CHAPTER_NUMBER, subjectChapter);
        frontCardIntent.putExtra(SUBJECT_PART, subjectPart);
        startActivity(frontCardIntent);
        // make transition push left
        referenceActivity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        referenceActivity.finish();
    }
}
