package com.olafss.shamscorner.olafss_flashcard.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "user")
public class UserData {

    /** user id */
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "user_id")
    private String mUserId;

    /** name of the user */
    @ColumnInfo(name = "name")
    private String mName;

    /** email id of the user */
    @ColumnInfo(name = "email")
    private String mEmail;

    /** phone number of the user */
    @ColumnInfo(name = "phone")
    private String mPhoneNumber;

    /** gender of the user (0-male, 1-female, 2-others) */
    @ColumnInfo(name = "gender")
    private int mGender;

    /** date of birth of the user (day-month-year) */
    @ColumnInfo(name = "date_of_birth")
    private String mDateOfBirth;

    /** profile image of the user */
    @ColumnInfo(name = "profile_image")
    private String mProfileImage;

    /** address of the user */
    @ColumnInfo(name = "address")
    private String mAddress;

    /** level of the user */
    @ColumnInfo(name = "level")
    private int mLevel;

    /** the paid or not */
    @ColumnInfo(name = "paid")
    private int mPaid;

    /**
     * the user data of the user table in the database
     * @param mUserId user id
     * @param mName user name
     * @param mEmail user email
     * @param mGender user gender (0-male, 1-female, 2-others)
     * @param mDateOfBirth user date of birth (01-01-1994)
     * @param mProfileImage user profile picture (url)
     * @param mAddress user address (city)
     * @param mLevel user current level (0-beginner, 1-advanced, 2-professional)
     * @param mPaid user paid option (0-not paid, 1-paid)
     */
    public UserData(@NonNull String mUserId, String mName, String mEmail, String mPhoneNumber,
                    int mGender, String mDateOfBirth, String mProfileImage,
                    String mAddress, int mLevel, int mPaid) {

        this.mUserId = mUserId;
        this.mName = mName;
        this.mEmail = mEmail;
        this.mPhoneNumber = mPhoneNumber;
        this.mGender = mGender;
        this.mDateOfBirth = mDateOfBirth;
        this.mProfileImage = mProfileImage;
        this.mAddress = mAddress;
        this.mLevel = mLevel;
        this.mPaid = mPaid;
    }

    public String getUserId() {
        return this.mUserId;
    }

    public String getName() {
        return this.mName;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public int getGender() {
        return this.mGender;
    }

    public String getDateOfBirth() {
        return this.mDateOfBirth;
    }

    public String getProfileImage() {
        return this.mProfileImage;
    }

    public String getAddress() {
        return this.mAddress;
    }

    public int getLevel() {
        return this.mLevel;
    }

    public int getPaid() {
        return this.mPaid;
    }

    public void setUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public void setGender(int mGender) {
        this.mGender = mGender;
    }

    public void setDateOfBirth(String mDateOfBirth) {
        this.mDateOfBirth = mDateOfBirth;
    }

    public void setProfileImage(String mProfileImage) {
        this.mProfileImage = mProfileImage;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public void setLevel(int mLevel) {
        this.mLevel = mLevel;
    }

    public void setPaid(int mPaid) {
        this.mPaid = mPaid;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String mPhoneNumber) {
        this.mPhoneNumber = mPhoneNumber;
    }
}
