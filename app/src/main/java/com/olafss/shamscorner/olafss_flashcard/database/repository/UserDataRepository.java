package com.olafss.shamscorner.olafss_flashcard.database.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.olafss.shamscorner.olafss_flashcard.database.FlashCardRoomDatabase;
import com.olafss.shamscorner.olafss_flashcard.database.dao.UserDataDao;
import com.olafss.shamscorner.olafss_flashcard.database.data.NameAndLevel;
import com.olafss.shamscorner.olafss_flashcard.database.entity.UserData;

public class UserDataRepository {

    /**
     * add the dao object
     */
    private UserDataDao mUserDataDao;

    /**
     * construct the repository for accessing the user data
     *
     * @param application the parent application object
     */
    public UserDataRepository(Application application) {
        // get the database
        FlashCardRoomDatabase db = FlashCardRoomDatabase.getDatabase(application);

        // get and set the dao
        mUserDataDao = db.userDataDao();
    }

    /**
     * get the all data of the user
     *
     * @return the user data
     */
    public UserData getUserData() {
        return mUserDataDao.getUserData();
    }

    /**
     * get the name and the level as live data
     *
     * @return the name and the level object of the live data
     */
    public LiveData<NameAndLevel> getNameAndLevel() {
        return mUserDataDao.getNameAndLevel();
    }

    /**
     * delete all the data of the user
     */
    public void deleteUserData() {
        new DeleteUserDataAsyncTask(mUserDataDao).execute();
    }

    private static class DeleteUserDataAsyncTask extends AsyncTask<Void, Void, Void> {

        private UserDataDao dataDao;

        DeleteUserDataAsyncTask(UserDataDao dataDao) {
            this.dataDao = dataDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            dataDao.deleteUserData();

            return null;
        }
    }

    /**
     * insert a row in the user table in the background
     *
     * @param userData the user data
     */
    public void insert(UserData userData) {
        new insertAsyncTask(mUserDataDao).execute(userData);
    }

    /**
     * insert the user data in the user table in background
     */
    private static class insertAsyncTask extends AsyncTask<UserData, Void, Void> {

        /**
         * the user data dao
         */
        private UserDataDao mAsyncTaskDao;

        insertAsyncTask(UserDataDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final UserData... userData) {
            mAsyncTaskDao.insertUser(userData[0]);
            return null;
        }
    }

    /**
     * get the user id
     *
     * @return the user id
     */
    public String getUserId() {
        return mUserDataDao.getUserId();
    }

    /**
     * get the user level as live data
     *
     * @return the user level
     */
    public LiveData<Integer> getUserLevel() {
        return mUserDataDao.getUserLevel();
    }
}
