package com.olafss.shamscorner.olafss_flashcard.utilities;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.olafss.shamscorner.olafss_flashcard.config.AppConfig;
import com.olafss.shamscorner.olafss_flashcard.view.activity.SampleDemo;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DownloadUtils {

    private static final String TAG = SampleDemo.class.getSimpleName();

    public long downloadZipFile(Context context, DownloadManager downloadManager) {

        // set the request
        DownloadManager.Request request = new DownloadManager.Request(
                Uri.parse(AppConfig.URL_ZIP + AppConfig.FILE_NAME_ZIP));

        //Set the title of this download, to be displayed in notifications (if enabled).
        request.setTitle("Olafss Flashcard");

        // set the description of this download
        request.setDescription("Data downloading...");

        // download it silently
        request.setVisibleInDownloadsUi(false);

        // set in the external storage in the download directory
        request.setDestinationInExternalFilesDir(context,
                Environment.getRootDirectory().getPath(), AppConfig.FILE_NAME_ZIP);

        //Enqueue a new download and same the referenceId
        long downloadZipId = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            downloadZipId = Objects.requireNonNull(downloadManager).enqueue(request);
        } else {
            if (downloadManager != null) {
                downloadZipId = downloadManager.enqueue(request);
            }
        }

        return downloadZipId;
    }

    public void deleteAndUnzip(String path, String zipname, DownloadManager downloadManager,
                               long zipId, String parentDirectoryName){
        // now delete and zip the file
        new DeleteAndUnZipAsyncTask(path, zipname, downloadManager, zipId, parentDirectoryName).execute();
    }

    private static class DeleteAndUnZipAsyncTask extends AsyncTask<Void, Void, Void> {

        private String path;
        private DownloadManager downloadManager;
        private long zipId;
        private String zipname;
        private String parentDirectoryName;

        DeleteAndUnZipAsyncTask(String path, String zipname, DownloadManager downloadManager,
                                long zipId, String parentDirectoryName){
            this.path = path;
            this.downloadManager = downloadManager;
            this.zipId = zipId;
            this.zipname = zipname;
            this.parentDirectoryName = parentDirectoryName;
        }

        @Override
        protected void onPreExecute() {

            deleteRecursive(new File(path + parentDirectoryName));

            Log.d(TAG, "previous directory deleted");
        }

        @Override
        protected Void doInBackground(Void... voids) {

            Log.d(TAG, "zipping started");

            InputStream is;
            ZipInputStream zis;
            try {
                String filename;
                is = new FileInputStream(path + zipname);
                zis = new ZipInputStream(new BufferedInputStream(is));
                ZipEntry ze;
                byte[] buffer = new byte[1024];
                int count;

                while ((ze = zis.getNextEntry()) != null) {
                    // get the zip file
                    filename = ze.getName();

                    // Need to create directories if not exists, or
                    // it will generate an Exception...
                    if (ze.isDirectory()) {
                        File fmd = new File(path + filename);
                        fmd.mkdirs();
                        continue;
                    }

                    FileOutputStream fout = new FileOutputStream(path + filename);

                    while ((count = zis.read(buffer)) != -1) {
                        fout.write(buffer, 0, count);
                    }

                    fout.close();
                    zis.closeEntry();
                }

                zis.close();

                Log.d(TAG, "zipping completed");
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // delete the zip file - because we no longer needed that
            downloadManager.remove(zipId);
        }

        private void deleteRecursive(File fileOrDirectory) {
            if (fileOrDirectory.isDirectory())
                for (File child : fileOrDirectory.listFiles())
                    deleteRecursive(child);

            fileOrDirectory.delete();
        }
    }
}
