package com.olafss.shamscorner.olafss_flashcard.view.activity;

import android.app.DownloadManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.config.AppConfig;
import com.olafss.shamscorner.olafss_flashcard.config.AppController;
import com.olafss.shamscorner.olafss_flashcard.database.entity.UserData;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.QuestionViewModel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.UserViewModel;
import com.olafss.shamscorner.olafss_flashcard.utilities.DownloadUtils;
import com.olafss.shamscorner.olafss_flashcard.utilities.SessionManager;
import com.olafss.shamscorner.olafss_flashcard.view.controller.DoThingsForLogin;
import com.olafss.shamscorner.olafss_flashcard.view.controller.SliderAdapterForIntro;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {

    private static final String TAG = Login.class.getSimpleName();

    /**
     * for the google sign in purpose
     */
    private static final int RC_SIGN_IN = 0;

    /**
     * the sign in with google button
     */
    private View btnSignInWithGoogle;

    /**
     * the sign in with facebook button
     */
    private View btnSignInWithFacebook;

    /**
     * the google api client
     */
    private GoogleApiClient mGoogleApiClient;

    /**
     * the progress bar to show for the log in
     */
    private ProgressBar progressBar;

    /**
     * the do things for the login object
     */
    private DoThingsForLogin doThingsForLogin;

    /**
     * the session manager for the shared pref
     */
    private SessionManager session;

    /**
     * is first time launch or not ( 0 - no, 1 - yes)
     */
    private int isFirstTimeLaunch;

    /**
     * the download manger for downloading the necessary data from the server
     */
    private DownloadManager downloadManager;

    /**
     * the intro view pager for the slider
     */
    private ViewPager viewPager;

    /**
     * the corresponding images for the intro
     */
    private List<Integer> imageIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(this, LauncherActivity.class);
            startActivity(intent);
            finish();
        }

        // set the content view
        setContentView(R.layout.activity_login);

        // get the view pager and the tab layout for the slider intro
        viewPager = findViewById(R.id.viewPager);

        // the indicator dot for the slider intro
        TabLayout indicator = findViewById(R.id.indicator);

        // set the image drawables for the image list
        imageIds = new ArrayList<>();
        imageIds.add(R.drawable.icon_info_group);
        imageIds.add(R.drawable.icon_level_up_group);
        imageIds.add(R.drawable.icon_finished_group);
        imageIds.add(R.drawable.icon_join_group);

        // set the slider adapter for the intro
        viewPager.setAdapter(new SliderAdapterForIntro(
                getApplicationContext(), imageIds));

        // set the indicator for the slider intro
        indicator.setupWithViewPager(viewPager, true);

        // now set the timer for the slider intro flip
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);

        // get the sign in with google button
        btnSignInWithGoogle = findViewById(R.id.sign_in_with_google);
        btnSignInWithGoogle.setOnClickListener(this);

        // get the sign in with facebook button
        btnSignInWithFacebook = findViewById(R.id.sign_in_with_facebook);
        btnSignInWithFacebook.setOnClickListener(this);

        // Build a GoogleApiClient
        mGoogleApiClient = buildGoogleApiClient();

        // get the progress bar
        progressBar = findViewById(R.id.progress_login);

        // check if it is first time launch or not
        if (session.isFirstTimeLaunch()) {
            isFirstTimeLaunch = 1;

            Log.i("IS_FIRST_TIME_LAUNCH", "yes");
        } else {
            isFirstTimeLaunch = 2;
            Log.i("IS_FIRST_TIME_LAUNCH", "no");
        }

        // get the download manager from android system
        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

        // intent for the broadcast receiver
        String downloadCompleteIntentName = DownloadManager.ACTION_DOWNLOAD_COMPLETE;
        IntentFilter downloadCompleteIntentFilter = new IntentFilter(downloadCompleteIntentName);

        // register the receiver
        registerReceiver(downloadCompleteReceiver, downloadCompleteIntentFilter);

    }

    /**
     * go to the register activity
     *
     * @param view the clicked view
     */
    public void clickRegisterButton(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * go to the login activity
     *
     * @param view the clicked view
     */
    public void clickLoginButton(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

        // check already signed in or not
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            // yes signed in
            updateUi(true);

            // Launch main activity
            Intent launcherIntent = new Intent(this, LauncherActivity.class);
            startActivity(launcherIntent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_with_google:
                // sign in with google
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                // update the ui to progress
                updateUi(true);
                break;

            case R.id.sign_in_with_facebook:
                // sign in with facebook
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        Log.i(TAG, "sign out");
                    }
                });
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            if (result.isSuccess()) {
                // get the account here...
                GoogleSignInAccount account = result.getSignInAccount();

                // handle the account
                handleGoogleSignInResult(account);

                Log.i(TAG, "connection success");
            } else {
                Log.i(TAG, "connection failed 2");
                updateUi(false);
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // update the ui
        Log.i(TAG, "connection failed 3");
        Toast.makeText(this, getString(R.string.wrong_msg_google_play_service),
                Toast.LENGTH_LONG).show();
        updateUi(false);
    }

    /**
     * handle the google sign in result
     *
     * @param account the google sign in account result
     */
    private void handleGoogleSignInResult(GoogleSignInAccount account) {

        if (account != null) {
            String name = account.getDisplayName();
            String email = account.getEmail();
            Uri photoUrl = account.getPhotoUrl();

            String photoUrlString = null;
            if (photoUrl != null) {
                photoUrlString = photoUrl.toString();
            }

            Log.i(TAG, "name - " + name +
                    ", email - " + email +
                    ", photoUrl - " + photoUrlString);

            // TODO: save the url of the picture into the shared pref and load into the glide

            // TODO: pass into the server now
            registerUser(name, "", "", "", email, "", photoUrlString);
        }
    }

    /**
     * get the google api client
     *
     * @return the google api client object
     */
    private GoogleApiClient buildGoogleApiClient() {
        // the google sign in option
        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();

        return new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                .build();
    }

    /**
     * show the progress bar
     */
    private void updateUi(boolean b) {
        if (b) {
            progressBar.setVisibility(View.VISIBLE);
            btnSignInWithGoogle.setEnabled(false);
        } else {
            progressBar.setVisibility(View.GONE);
            btnSignInWithGoogle.setEnabled(true);
        }
    }

    /**
     * register the user in the server
     *
     * @param name        user name
     * @param gender      user gender
     * @param dateOfBirth user date of birth
     * @param address     user address
     * @param email       user email address
     * @param phone       user phone number
     */
    private void registerUser(final String name, final String gender, final String dateOfBirth,
                              final String address, final String email, final String phone,
                              final String url) {

        // Tag used to cancel the request
        String tag_string_req = "req_register";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTER_ONE_CLICK, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {

                        // get the username of the user
                        String username = jObj.getString("username");

                        // User successfully stored in MySQL
                        Toast.makeText(getApplicationContext(), R.string.successfully_logged_in,
                                Toast.LENGTH_LONG).show();

                        // initiate the question model for the question table
                        QuestionViewModel mQuestionViewModel = ViewModelProviders.of(Login.this)
                                .get(QuestionViewModel.class);

                        // initiate the do things for login object
                        doThingsForLogin = new DoThingsForLogin(Login.this, mQuestionViewModel);

                        // check the login
                        checkLogin(email, username);
                    } else {
                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();

                        // hide the progressbar
                        updateUi(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    // hide the progressbar
                    updateUi(false);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_connection),
                        Toast.LENGTH_LONG).show();

                // hide the progressbar
                updateUi(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("gender", gender);
                params.put("date_of_birth", dateOfBirth);
                params.put("address", address);
                params.put("email", email);
                params.put("phone", phone);
                params.put("image_url", url);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * method to verify login details in mysql database
     *
     * @param email the user email
     */
    private void checkLogin(final String email, final String usernameParam) {

        // Tag used to cancel the request
        String tag_string_req = "req_login";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGIN_ONE_CLICK, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // get the user json object
                        JSONObject user = jObj.getJSONObject("user");
                        // get the user name from the server
                        String username = user.getString("username");

                        // initiate the view model for the user table
                        UserViewModel mUserViewModel = ViewModelProviders.of(Login.this)
                                .get(UserViewModel.class);

                        // first delete the current user
                        mUserViewModel.deleteUserData();

                        // Inserting row in users table
                        mUserViewModel.insert(new UserData(
                                username,
                                user.getString("name"),
                                user.getString("email"),
                                user.getString("phone"),
                                Integer.parseInt(user.getString("gender")),
                                user.getString("date_of_birth"),
                                user.getString("image_url"),
                                user.getString("address"),
                                Integer.parseInt(user.getString("level")),
                                Integer.parseInt(user.getString("paid"))));

                        // sync and add the question table from the server
                        doThingsForLogin.syncQuestions(username, isFirstTimeLaunch, downloadManager, null, progressBar);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();

                        updateUi(false);
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.something_wrong),
                            Toast.LENGTH_LONG).show();

                    updateUi(false);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_connection),
                        Toast.LENGTH_LONG).show();

                updateUi(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("username", usernameParam);
                params.put("sub_code", AppConfig.APP_SUBJECT_CODE);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private BroadcastReceiver downloadCompleteReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            long downloadZipId = doThingsForLogin.getDownloadZipId();

            if (referenceId == downloadZipId) {

                Log.i(TAG, "files downloaded");

                String path;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    path = Objects.requireNonNull(getExternalFilesDir(Environment.getRootDirectory().getPath()))
                            .getPath() + "/";
                } else {
                    path = "/storage/emulated/0/Android/data/com.olafss.shamscorner.olafss_flashcard/files/system/";
                }

                // then unzip and delete the downloaded files
                new DownloadUtils().deleteAndUnzip(path, AppConfig.FILE_NAME_ZIP,
                        downloadManager, downloadZipId, AppConfig.IMAGE_LOCATION_DIRECTORY);

            } else {
                Log.d(TAG, "id not matched");
            }

            // Launch main activity
            Intent launcherIntent = new Intent(context, LauncherActivity.class);
            startActivity(launcherIntent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            finish();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (downloadCompleteReceiver != null) {
            // stop receiving the response for the broadcast receiver
            unregisterReceiver(downloadCompleteReceiver);
        }
    }

    /**
     * the auto slider time tuner for the intro
     */
    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            Login.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < imageIds.size() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}
