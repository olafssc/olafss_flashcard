package com.olafss.shamscorner.olafss_flashcard.view.controller;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ProgressData;
import com.olafss.shamscorner.olafss_flashcard.utilities.ConvertEngToBang;

import java.util.List;

public class ListItemRecycleViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * the question list
     */
    private List<ProgressData> progressList;

    /**
     * the position of the part - 2
     */
    private int nextPartPosition;

    /**
     * how many times it will increment
     */
    private int incrementCounter;

    private Context context;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == 0) {

            // return the view holder for displaying the part
            return new ListItemRecycleViewAdapter.PartViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_fragment_home_just_text, parent, false));

        } else {

            // return the view holder for displaying the progress bar
            return new ListItemRecycleViewAdapter.ListItemViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_fragment_home, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        if (position == 0) {
            PartViewHolder holderThis = (PartViewHolder) holder;
            holderThis.tvPart.setText(context.getResources().getString(R.string.part_1));
            incrementCounter = 1;
        } else if (position == nextPartPosition) {
            PartViewHolder holderThis = (PartViewHolder) holder;
            holderThis.tvPart.setText(context.getResources().getString(R.string.part_2));
            incrementCounter = 2;
        } else {
            // cast the holder to the list holder type
            ListItemViewHolder holderThis = (ListItemViewHolder) holder;

            // get the progress object from the position
            ProgressData progressData = progressList.get(position - incrementCounter);

            // set the chapter name text view
            String chapterName = context.getResources().getString(R.string.chapter) + " "
                    + ConvertEngToBang.getNumberInBanglaFormat(progressData.getPart())
                    + "." + ConvertEngToBang.getNumberInBanglaFormat(progressData.getChapterNumber())
                    + " - " + progressData.getChapterName();
            holderThis.tvChapterName.setText(chapterName);

            // set the new, learning, review and master progress value
            holderThis.progressBarNew.setProgress(progressData.getProgressNew());
            holderThis.progressBarLearning.setProgress(progressData.getProgressLearning());
            holderThis.progressBarReview.setProgress(progressData.getProgressReview());
            holderThis.progressBarMaster.setProgress(progressData.getProgressMaster());

            // set the percent text
            String tNew = progressData.getProgressNew() + "%";
            holderThis.tvNew.setText(tNew);
            String tLearning = progressData.getProgressLearning() + "%";
            holderThis.tvLearning.setText(tLearning);
            String tReview = progressData.getProgressReview() + "%";
            holderThis.tvReview.setText(tReview);
            String tMaster = progressData.getProgressMaster() + "%";
            holderThis.tvMaster.setText(tMaster);

            // set the complete image view state
            if (progressData.getProgressMaster() == 100) {
                holderThis.complete.setImageResource(R.drawable.icon_done);
                holderThis.complete.setContentDescription(context.getResources().getString(R.string.ok));
            } else {
                holderThis.complete.setImageResource(R.drawable.icon_clear);
                holderThis.complete.setContentDescription(context.getResources().getString(R.string.no));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 || position == nextPartPosition) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return progressList.size() + 2;
    }

    class ListItemViewHolder extends RecyclerView.ViewHolder {
        /**
         * the chapter name text view
         */
        TextView tvChapterName;

        /**
         * the new progress percent
         */
        TextView tvNew;

        /**
         * the learning progress percent
         */
        TextView tvLearning;

        /**
         * the review progress percent
         */
        TextView tvReview;

        /**
         * the master progress percent
         */
        TextView tvMaster;

        /**
         * the new progress bar
         */
        ProgressBar progressBarNew;

        /**
         * the learning progress bar
         */
        ProgressBar progressBarLearning;

        /**
         * the review progress bar
         */
        ProgressBar progressBarReview;

        /**
         * the master progress bar
         */
        ProgressBar progressBarMaster;

        /**
         * the complete state image view
         */
        ImageView complete;

        /**
         * get the views
         *
         * @param view the parent view
         */
        ListItemViewHolder(View view) {
            super(view);

            // find the elements view by their id
            tvChapterName = view.findViewById(R.id.chapter_name);
            progressBarNew = view.findViewById(R.id.progressBarNew);
            progressBarLearning = view.findViewById(R.id.progressBarLearning);
            progressBarReview = view.findViewById(R.id.progressBarReview);
            progressBarMaster = view.findViewById(R.id.progressBarMaster);
            tvNew = view.findViewById(R.id.tv_percentNew);
            tvLearning = view.findViewById(R.id.tv_percentLearning);
            tvReview = view.findViewById(R.id.tv_percentReview);
            tvMaster = view.findViewById(R.id.tv_percentMaster);
            complete = view.findViewById(R.id.view_complete);
        }
    }

    class PartViewHolder extends RecyclerView.ViewHolder {

        /**
         * text view for showing the part information
         */
        TextView tvPart;

        PartViewHolder(View itemView) {
            super(itemView);

            tvPart = itemView.findViewById(R.id.tv_part_header);
        }
    }

    /**
     * set the recycler view items into the panel
     *
     * @param progressList the list of progress
     */
    public void setItems(Context context, List<ProgressData> progressList, int nextPartPosition) {
        this.context = context;
        this.progressList = progressList;
        this.nextPartPosition = nextPartPosition;
        // notify data set changed for automatic update
        notifyDataSetChanged();
    }
}
