package com.olafss.shamscorner.olafss_flashcard.view.controller;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.data.OthersData;

import java.util.List;

public class ListItemRecyclerOthers extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * the other data list for the recycler view
     */
    private List<OthersData> othersDataList;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ListItemRecyclerOthers.OthersDataViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_others, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        // get the main holder as list item view
        OthersDataViewHolder holderThis = (OthersDataViewHolder) holder;

        // get the other data
        OthersData othersData = othersDataList.get(position);

        // set the text into the text view
        holderThis.textView.setText(othersData.text);
        // set the image into the image view
        holderThis.imageView.setImageResource(othersData.imageId);
    }

    @Override
    public int getItemCount() {
        return othersDataList.size();
    }

    /**
     * holder inner class for the each item
     */
    class OthersDataViewHolder extends RecyclerView.ViewHolder {

        /**
         * the text view for the item's title
         */
        TextView textView;

        /**
         * the image field for the item
         */
        ImageView imageView;

        OthersDataViewHolder(View itemView) {
            super(itemView);

            // get each view for each item
            textView = itemView.findViewById(R.id.text);
            imageView = itemView.findViewById(R.id.image);
        }
    }

    /**
     * set the recycler view items into the panel
     *
     * @param othersDataList the list of the others data in the other section
     */
    public void setItems(List<OthersData> othersDataList) {
        this.othersDataList = othersDataList;
    }
}
