package com.olafss.shamscorner.olafss_flashcard.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.olafss.shamscorner.olafss_flashcard.database.data.ProgressValue;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ProgressData;

import java.util.List;

@Dao
public interface ProgressDataDao {

    /** get the all information of the progress table */
    @Query("SELECT * FROM progress ORDER BY part, chapter_number")
    LiveData<List<ProgressData>> getProgressData();

    /** get a row from the progress table */
    @Query("SELECT new, learning, review, master FROM progress WHERE chapter_number = :chapter AND part = :part")
    ProgressValue getAProgressData(int chapter, int part);

    /** delete the current progress data */
    @Query("DELETE FROM progress")
    void deleteProgressData();

    /** get the total number of progress count */
    @Query("SELECT COUNT(id) FROM progress")
    int getProgressDataCount();

    /** insert the progress data into the progress table */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProgressList(List<ProgressData> progressDataList);

    /** get the chapter number of part 1 */
    @Query("SELECT COUNT(id) FROM chapters WHERE part = 1")
    int getChapterCountForPartOne();

    /** reset the progress data in the specified chapter and part */
    @Query("UPDATE progress SET new = 100, learning = 0, review = 0, master = 0 WHERE chapter_number = :chapter AND part = :part")
    void resetProgressData(int chapter, int part);

    /** update the progress data into the given chapter, part */
    @Query("UPDATE progress SET new = :newVal, learning = :learningVal, review = :reviewVal, master = :masterVal" +
            " WHERE chapter_number = :chapter AND part = :part")
    void updateProgressData(int chapter, int part, int newVal, int learningVal, int reviewVal, int masterVal);

    /** get the progress master value */
    @Query("SELECT master FROM progress WHERE chapter_number = :chapter AND part = :part")
    int getMasterProgressValue(int chapter, int part);
}
