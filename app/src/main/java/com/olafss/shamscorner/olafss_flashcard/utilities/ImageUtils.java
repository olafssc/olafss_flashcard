package com.olafss.shamscorner.olafss_flashcard.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.olafss.shamscorner.olafss_flashcard.config.AppConfig;
import com.olafss.shamscorner.olafss_flashcard.config.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

public class ImageUtils {

    private static final String TAG = ImageUtils.class.getSimpleName();

    /**
     * the application context
     */
    private Context mContext;

    public ImageUtils(Context context) {
        this.mContext = context;
    }

    /**
     * make the bitmap to a perfect square
     *
     * @param bitmap the bitmap image to be cropped
     * @return the cropped from of the bitmap
     */
    public Bitmap cropToSquareImage (Bitmap bitmap) {
        // the cropped bitmap
        Bitmap dstBmp;
        if (bitmap.getWidth() >= bitmap.getHeight()){

            dstBmp = Bitmap.createBitmap(
                    bitmap,
                    bitmap.getWidth()/2 - bitmap.getHeight()/2,
                    0,
                    bitmap.getHeight(),
                    bitmap.getHeight()
            );

        }else{

            dstBmp = Bitmap.createBitmap(
                    bitmap,
                    0,
                    bitmap.getHeight()/2 - bitmap.getWidth()/2,
                    bitmap.getWidth(),
                    bitmap.getWidth()
            );
        }

        // return the destination bitmap
        return dstBmp;
    }

    /**
     * compress and save the image file into the external storage directory
     *
     * @param finalBitmap the final image bitmap file
     */
    public void saveImage(Bitmap finalBitmap, String imagePath) {
        File file = new File(imagePath);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 15, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(mContext, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });
    }

    /**
     * get the string format of a jpeg image
     *
     * @param dstBmp the bitmap image to be converted to string
     * @return string format of the given bitmap
     */
    public String getStringImage(Bitmap dstBmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        dstBmp.compress(Bitmap.CompressFormat.JPEG, 20, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    /**
     * upload the image to the server
     *
     * @param imageFile the image file converted to string
     * @param imageName the image file name
     */
    /*
    public void uploadImageToServer (final String imageFile, final String imageName) {
        // encode the image to a json object
        final JSONObject imageObject = new JSONObject();
        try {
            imageObject.put("size", "1000");
            imageObject.put("type", "image/jpg");
            imageObject.put("data", imageFile);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Tag used to cancel the request
        String tag_string_req = "req_upload_image";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_IMAGE_UPLOAD, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Upload Response: " + response);

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        Log.d(TAG, "Upload successful");
                    } else {
                        Log.d(TAG, "Upload failed");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<>();
                params.put("image", String.valueOf(imageObject));
                params.put("image_name", imageName);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    */
}
