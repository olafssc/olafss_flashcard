package com.olafss.shamscorner.olafss_flashcard.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "questions")
public class QuestionData implements Parcelable {

    /** the question unique id */
    @PrimaryKey
    @ColumnInfo(name = "unique_id")
    private int mUniqueId;

    /** the question name */
    @ColumnInfo(name = "name")
    private String mName;

    /** the corresponding question answer */
    @ColumnInfo(name = "answer")
    private String mAnswer;

    /** the state of the question(0-new, 1-learning, 2-review, 3-master) */
    @ColumnInfo(name = "state")
    private int mState;

    /** the number of times the user clicked on the i_know button */
    @ColumnInfo(name = "i_know")
    private int mIKnow;

    /** the number of times the user clicked on the i_dont_know button */
    @ColumnInfo(name = "i_dont_know")
    private int mIDontKnow;

    /** the number of times the user flipped this question */
    @ColumnInfo(name = "flipped")
    private int mFlipped;

    /** the number of times the user skipped this question */
    @ColumnInfo(name = "skipped")
    private int mSkipped;

    /** the subject part of this question */
    @ColumnInfo(name = "part")
    private int mPart;

    /** the subject chapter of this question */
    @ColumnInfo(name = "chapter")
    private int mChapter;

    /** the question is paid or not */
    @ColumnInfo(name = "paid")
    private int mPaid;

    /**
     * construct the question data from the question table in the database
     * @param mUniqueId question unique id from the server
     * @param mName question name
     * @param mAnswer question answer
     * @param mState state of the user
     * @param mIKnow i know counter
     * @param mIDontKnow i don't know counter
     * @param mFlipped flipped counter
     * @param mSkipped skipped counter
     * @param mPart subject part of the question
     * @param mChapter chapter number of the question
     * @param mPaid 0-free question, 1-paid question
     */
    public QuestionData(int mUniqueId, String mName, String mAnswer, int mState,
                        int mIKnow, int mIDontKnow, int mFlipped, int mSkipped, int mPart, int mChapter,
                        int mPaid) {

        this.mUniqueId = mUniqueId;
        this.mName = mName;
        this.mAnswer = mAnswer;
        this.mState = mState;
        this.mIKnow = mIKnow;
        this.mIDontKnow = mIDontKnow;
        this.mFlipped = mFlipped;
        this.mSkipped = mSkipped;
        this.mPart = mPart;
        this.mChapter = mChapter;
        this.mPaid = mPaid;
    }

    public int getUniqueId() {
        return this.mUniqueId;
    }

    public String getName() {
        return this.mName;
    }

    public String getAnswer() {
        return this.mAnswer;
    }

    public int getState() {
        return this.mState;
    }

    public int getIKnow() {
        return this.mIKnow;
    }

    public int getIDontKnow() {
        return this.mIDontKnow;
    }

    public int getFlipped() {
        return this.mFlipped;
    }

    public int getSkipped() {
        return this.mSkipped;
    }

    public int getPart() {
        return this.mPart;
    }

    public int getChapter() {
        return this.mChapter;
    }

    public int getPaid() {
        return this.mPaid;
    }

    public void setUniqueId(int mUniqueId) {
        this.mUniqueId = mUniqueId;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public void setAnswer(String mAnswer) {
        this.mAnswer = mAnswer;
    }

    public void setState(int mState) {
        this.mState = mState;
    }

    public void setIKnow(int mIKnow) {
        this.mIKnow = mIKnow;
    }

    public void setIDontKnow(int mIDontKnow) {
        this.mIDontKnow = mIDontKnow;
    }

    public void setFlipped(int mFlipped) {
        this.mFlipped = mFlipped;
    }

    public void setSkipped(int mSkipped) {
        this.mSkipped = mSkipped;
    }

    public void setPart(int mPart) {
        this.mPart = mPart;
    }

    public void setChapter(int mChapter) {
        this.mChapter = mChapter;
    }

    public void setPaid(int mPaid) {
        this.mPaid = mPaid;
    }

    // parcelable contents

    @Ignore
    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mUniqueId);
        dest.writeString(this.mName);
        dest.writeString(this.mAnswer);
        dest.writeInt(this.mState);
        dest.writeInt(this.mIKnow);
        dest.writeInt(this.mIDontKnow);
        dest.writeInt(this.mFlipped);
        dest.writeInt(this.mSkipped);
        dest.writeInt(this.mPart);
        dest.writeInt(this.mChapter);
        dest.writeInt(this.mPaid);
    }

    @Ignore
    private QuestionData(Parcel in) {
        this.mUniqueId = in.readInt();
        this.mName = in.readString();
        this.mAnswer = in.readString();
        this.mState = in.readInt();
        this.mIKnow = in.readInt();
        this.mIDontKnow = in.readInt();
        this.mFlipped = in.readInt();
        this.mSkipped = in.readInt();
        this.mPart = in.readInt();
        this.mChapter = in.readInt();
        this.mPaid = in.readInt();
    }

    @Ignore
    public static final Creator<QuestionData> CREATOR = new Creator<QuestionData>() {
        @Override
        public QuestionData createFromParcel(Parcel source) {
            return new QuestionData(source);
        }

        @Override
        public QuestionData[] newArray(int size) {
            return new QuestionData[size];
        }
    };

    @Ignore
    @Override
    public String toString() {
        return "QuestionData{" +
                "mUniqueId=" + mUniqueId +
                ", mName='" + mName + '\'' +
                ", mAnswer='" + mAnswer + '\'' +
                ", mState=" + mState +
                ", mIKnow=" + mIKnow +
                ", mIDontKnow=" + mIDontKnow +
                ", mFlipped=" + mFlipped +
                ", mSkipped=" + mSkipped +
                ", mPart=" + mPart +
                ", mChapter=" + mChapter +
                ", mPaid=" + mPaid +
                '}';
    }
}