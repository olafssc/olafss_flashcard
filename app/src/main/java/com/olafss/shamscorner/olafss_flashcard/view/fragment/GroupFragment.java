package com.olafss.shamscorner.olafss_flashcard.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.data.NewsFeedData;
import com.olafss.shamscorner.olafss_flashcard.view.controller.ListItemRecycleNewsFeed;

import java.util.ArrayList;
import java.util.List;

public class GroupFragment extends Fragment {

    public GroupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View parentHolder =  inflater.inflate(R.layout.fragment_group, container, false);

        // initiate the recycle view
        /* the recycle view for the news feed contents */
        RecyclerView recyclerView = parentHolder.findViewById(R.id.news_feed_list);

        // set the recycle view contents
        /* the adapter for the news feed view */
        ListItemRecycleNewsFeed mAdapter = new ListItemRecycleNewsFeed();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // sample list item
        // todo: change this into dynamic paging list
        List<NewsFeedData> itemList = new ArrayList<>();
        itemList.add(new NewsFeedData("Congratulations!\nA card has been finished.\nPart - 1, Chapter - 5\nKnowing-ratio: 50%",
                R.drawable.icon_finished_group));
        itemList.add(new NewsFeedData("Attention!\nA new card has been added.\nPart - 1, Chapter - 8",
                R.drawable.icon_info_group));
        itemList.add(new NewsFeedData("Welcome!\nMD Jafar Sarkar\nI hope you will get the best benefit from this community.",
                R.drawable.icon_join_group));
        itemList.add(new NewsFeedData("Congratulations!\nLevel up - Advanced.\nThank you for being with us.",
                R.drawable.icon_level_up_group));
        itemList.add(new NewsFeedData("Welcome!\nJenny Ahmed\nI hope you will get the best benefit from this community.",
                R.drawable.icon_join_group));
        itemList.add(new NewsFeedData("Congratulations!\nLevel up - Professional.\nThank you for being with us.",
                R.drawable.icon_level_up_group));
        itemList.add(new NewsFeedData("We are really sorry for some issues, we will solve this as soon as possible. " +
                "Thanks for your feedback.", R.drawable.icon_info_group));
        itemList.add(new NewsFeedData("Welcome!\nAsif Rokoni\nI hope you will get the best benefit from this community.",
                R.drawable.icon_join_group));
        itemList.add(new NewsFeedData("Congratulations!\nLevel up - Advanced.\nThank you for being with us.",
                R.drawable.icon_level_up_group));
        itemList.add(new NewsFeedData("Attention!\nFor the maintaining purposes, we will hold the server for a while. " +
                "But you can carry on.", R.drawable.icon_info_group));
        itemList.add(new NewsFeedData("Congratulations!\nA card has been finished.\nPart - 2, Chapter - 8\nKnowing-ratio: 90%",
                R.drawable.icon_finished_group));
        itemList.add(new NewsFeedData("Discount!\nWe are providing 75% discount on 6 months pack. So, grab this before " +
                "time runs out.", R.drawable.icon_info_group));
        itemList.add(new NewsFeedData("Welcome!\nAshish Mahanta\nI hope you will get the best benefit from this community.",
                R.drawable.icon_join_group));

        // set the item list into the adapter
        mAdapter.setItems(itemList);
        // set the adapter into the recycler view
        recyclerView.setAdapter(mAdapter);

        // return the parent view
        return parentHolder;
    }
}
