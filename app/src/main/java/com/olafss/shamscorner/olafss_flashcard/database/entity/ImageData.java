package com.olafss.shamscorner.olafss_flashcard.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "images")
public class ImageData {

    /** image id */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    /** the unique id */
    @ColumnInfo(name = "unique_id")
    private int mUniqueId;

    /** the question image url */
    @ColumnInfo(name = "url")
    private String mImageUrl;

    /** the image type (0-question, 1-answer) */
    @ColumnInfo(name = "type")
    private int mType;

    /** the question image position (0-bottom, 1-up) */
    @ColumnInfo(name = "position")
    private int mImagePosition;

    /**
     * construct the image data from the image table in the database
     * @param mUniqueId question unique id
     * @param mImageUrl the image url
     * @param mType the image type (0-question, 1-answer)
     * @param mImagePosition the image position in the question panel (0-bottom, 1-up)
     */
    public ImageData(int mUniqueId, String mImageUrl, int mType, int mImagePosition) {
        this.mUniqueId = mUniqueId;
        this.mImageUrl = mImageUrl;
        this.mType = mType;
        this.mImagePosition = mImagePosition;
    }

    public int getId() {
        return this.mId;
    }

    public int getUniqueId() {
        return this.mUniqueId;
    }

    public String getImageUrl() {
        return this.mImageUrl;
    }

    public int getType() {
        return this.mType;
    }

    public int getImagePosition() {
        return this.mImagePosition;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public void setUniqueId(int mUniqueId) {
        this.mUniqueId = mUniqueId;
    }

    public void setImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public void setType(int mType) {
        this.mType = mType;
    }

    public void setImagePosition(int mImagePosition) {
        this.mImagePosition = mImagePosition;
    }
}
