package com.olafss.shamscorner.olafss_flashcard.database.data;

import android.arch.persistence.room.ColumnInfo;

public class NameAndLevel {
    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "level")
    public int level;
}
