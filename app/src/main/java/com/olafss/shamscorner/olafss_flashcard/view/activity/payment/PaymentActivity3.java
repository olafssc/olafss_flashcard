package com.olafss.shamscorner.olafss_flashcard.view.activity.payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.baoyachi.stepview.HorizontalStepView;
import com.baoyachi.stepview.bean.StepBean;
import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.utilities.GoHome;

import java.util.ArrayList;
import java.util.List;

public class PaymentActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_step_3);

        // set the steps view
        HorizontalStepView setpview5 = findViewById(R.id.step_view);
        List<StepBean> stepsBeanList = new ArrayList<>();
        StepBean stepBean0 = new StepBean("Choose",1);
        StepBean stepBean1 = new StepBean("Pay",1);
        StepBean stepBean2 = new StepBean("Enter",-1);
        StepBean stepBean3 = new StepBean("Go",-1);
        stepsBeanList.add(stepBean0);
        stepsBeanList.add(stepBean1);
        stepsBeanList.add(stepBean2);
        stepsBeanList.add(stepBean3);
        setpview5
                .setStepViewTexts(stepsBeanList)
                .setTextSize(12)//set textSize
                .setStepsViewIndicatorCompletedLineColor(getResources().getColor(R.color.colorAccent))
                .setStepsViewIndicatorUnCompletedLineColor(getResources().getColor(R.color.colorAccent))
                .setStepViewComplectedTextColor(getResources().getColor(R.color.colorAccent))
                .setStepViewUnComplectedTextColor(getResources().getColor(R.color.colorAccent))
                .setStepsViewIndicatorCompleteIcon(getResources().getDrawable(R.drawable.icon_check_round))
                .setStepsViewIndicatorDefaultIcon(getResources().getDrawable(R.drawable.icon_cancel_round))
                .setStepsViewIndicatorAttentionIcon(getResources().getDrawable(R.drawable.icon_replay));
    }

    @Override
    public void onBackPressed() {
        new GoHome(this).showDialog();
    }

    /**
     * verify the transaction id
     * @param view the clicked button
     */
    public void onButtonClickedVerify(View view) {
        Intent intent = new Intent(this, PaymentActivity4.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        finish();
    }
}
