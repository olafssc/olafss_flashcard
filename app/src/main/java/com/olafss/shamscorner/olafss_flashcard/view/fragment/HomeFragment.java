package com.olafss.shamscorner.olafss_flashcard.view.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.olafss.shamscorner.olafss_flashcard.R;
import com.olafss.shamscorner.olafss_flashcard.database.entity.ProgressData;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.ProgressViewModel;
import com.olafss.shamscorner.olafss_flashcard.database.viewmodel.QuestionViewModel;
import com.olafss.shamscorner.olafss_flashcard.utilities.RecyclerTouchListener;
import com.olafss.shamscorner.olafss_flashcard.view.activity.CardActivity;
import com.olafss.shamscorner.olafss_flashcard.view.activity.FinishedActivity;
import com.olafss.shamscorner.olafss_flashcard.view.controller.ListItemRecycleViewAdapter;

import java.util.List;

public class HomeFragment extends Fragment {

    // the reference activity
    private Activity referenceActivity;

    /**
     * The key ID of the chapter number
     */
    private static final String CHAPTER_NUMBER = "CHAPTER_NUMBER";

    /**
     * The key ID of the subject part
     */
    private static final String SUBJECT_PART = "CHAPTER_PART";

    /**
     * the progress view model
     */
    private ProgressViewModel mProgressViewModel;

    /**
     * the question view model
     */
    private QuestionViewModel mQuestionViewModel;

    /**
     * the progress list
     */
    List<ProgressData> progressList;

    /**
     * the adapter for the list recycler view
     */
    private ListItemRecycleViewAdapter mAdapter;

    /**
     * home chapter list and progress recycler view
     */
    private RecyclerView recyclerView;

    /**
     * the number of chapter in the first part as position in recycler view
     */
    private int countChapterForPartOne;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // get the main activity
        referenceActivity = getActivity();

        // Inflate the layout for this fragment
        View parentHolder = inflater.inflate(R.layout.fragment_home, container, false);

        // initiate the recycle view
        /* the recycle view for the chapter contents */
        recyclerView = parentHolder.findViewById(R.id.chapter_list);

        // set the recycle view contents
        /* the adapter for the progress view */
        mAdapter = new ListItemRecycleViewAdapter();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(referenceActivity.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // get the view model for the progress data
        mProgressViewModel = ViewModelProviders.of(this).get(ProgressViewModel.class);

        // get the view model for the question data
        mQuestionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);

        // get the context
        final Fragment fragment = this;

        new GetChapterCountForPartOne(mProgressViewModel) {
            @Override
            public void onResponseReceive(final int count) {
                countChapterForPartOne = count + 1;
                mProgressViewModel.getProgressData().observe(fragment, new Observer<List<ProgressData>>() {
                    @Override
                    public void onChanged(@Nullable final List<ProgressData> progressData) {
                        progressList = progressData;
                        mAdapter.setItems(fragment.getContext(), progressData, count + 1);
                        recyclerView.setAdapter(mAdapter);
                    }
                });
            }
        }.execute();

        // toggle the empty list message if items available
        // get the text view for no content
        final TextView noContent = parentHolder.findViewById(R.id.no_content_view);

        new ToggleEmptyNotes(mProgressViewModel) {
            @Override
            public void onResponseReceive(int count) {
                //Log.i("HOME_FRAGMENT", ""+count);
                if (count > 0) {
                    noContent.setVisibility(View.GONE);
                } else {
                    noContent.setVisibility(View.VISIBLE);
                }
            }
        }.execute();

        // add the touch event
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(referenceActivity.getApplicationContext(),
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, final int position) {

                /*ImageView imgViewReset = view.findViewById(R.id.view_reset);
                imgViewReset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i("EXPE_RESE", "inner");
                    }
                });*/

                        if (position != 0 && position != countChapterForPartOne) {

                            // the position of the item
                            int itemPosition;
                            if (position < countChapterForPartOne) {
                                // decrement by 1
                                itemPosition = position - 1;
                            } else {
                                // decrement by 2
                                itemPosition = position - 2;
                            }

                            if (progressList.get(itemPosition).getProgressMaster() == 100) {

                                // open up the finishing dialog activity
                                Intent finishedIntent = new Intent(referenceActivity, FinishedActivity.class);
                                finishedIntent.putExtra(CHAPTER_NUMBER, progressList.get(itemPosition).getChapterNumber());
                                finishedIntent.putExtra(SUBJECT_PART, progressList.get(itemPosition).getPart());
                                referenceActivity.startActivity(finishedIntent);
                                // make transition push left
                                referenceActivity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

                            } else {

                                // pass the chapter number to the next activity using extras
                                Intent frontCardIntent = new Intent(referenceActivity, CardActivity.class);
                                frontCardIntent.putExtra(CHAPTER_NUMBER, progressList.get(itemPosition).getChapterNumber());
                                frontCardIntent.putExtra(SUBJECT_PART, progressList.get(itemPosition).getPart());

                                referenceActivity.startActivity(frontCardIntent);
                                referenceActivity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                            }

                            // finish the current activity
                            referenceActivity.finish();
                        }
                    }

            /*@Override
            public void onLongClick(View view, int position) {

                if (position != 0 && position != countChapterForPartOne) {

                    // the position of the item
                    int itemPosition;
                    if (position < countChapterForPartOne) {
                        // decrement by 1
                        itemPosition = position - 1;
                    } else {
                        // decrement by 2
                        itemPosition = position - 2;
                    }

                    // open up the reset dialog
                    resetDialog(progressList.get(itemPosition).getChapterNumber(),
                            progressList.get(itemPosition).getPart());
                }
            }*/
                }));

        // return the inflate layout of the fragment
        return parentHolder;
    }

    private interface ChapterCount {
        void onResponseReceive(int count);
    }

    private static abstract class GetChapterCountForPartOne extends AsyncTask<Void, Void, Void>
            implements ChapterCount {

        private ProgressViewModel mAsyncTask;
        private int count;

        GetChapterCountForPartOne(ProgressViewModel mAsyncTask) {
            this.mAsyncTask = mAsyncTask;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // get the count
            count = mAsyncTask.getChapterCountForPartOne();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            onResponseReceive(count);
        }
    }

    private interface ToggleNotes {
        void onResponseReceive(int count);
    }

    private static abstract class ToggleEmptyNotes extends AsyncTask<Void, Void, Void>
            implements ToggleNotes {

        private ProgressViewModel mProgressViewModel;
        private int count;

        ToggleEmptyNotes(ProgressViewModel mProgressViewModel) {
            this.mProgressViewModel = mProgressViewModel;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // get the count
            count = mProgressViewModel.getProgressDataCount();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            onResponseReceive(count);
        }
    }

    /**
     * reset card option dialog
     */
    private void resetDialog(final int chapter, final int part) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(referenceActivity);
        alertDialogBuilder.setMessage(R.string.dialog_reset);
        alertDialogBuilder.setPositiveButton(R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // reset the card
                        new UpdateThingsInBackgroundForResetCardData(chapter, part).execute();
                    }
                });

        alertDialogBuilder.setNegativeButton(R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // if it is not ok
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * the background process for reset the data for the specific card
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateThingsInBackgroundForResetCardData extends AsyncTask<Void, Void, Void> {

        // the subject chapter and the part which on the long click performs
        private int subjectChapter;
        private int subjectPart;

        UpdateThingsInBackgroundForResetCardData(int subjectChapter, int subjectPart) {
            this.subjectChapter = subjectChapter;
            this.subjectPart = subjectPart;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // reset the state of the specified chapter and part of a question in the questions table
            mQuestionViewModel.resetQuestionState(subjectChapter, subjectPart);

            // reset the progress table of the specified chapter and part
            mProgressViewModel.resetProgressData(subjectChapter, subjectPart);

            return null;
        }
    }
}
