package com.olafss.shamscorner.olafss_flashcard.database.data;

public class NewsFeedData {
    /**
     * the main text into the news feed
     */
    public String text;

    /**
     * the image id for the individual icon
     */
    public int imageId;

    public NewsFeedData(String text, int imageId) {
        this.text = text;
        this.imageId = imageId;
    }
}
